package main

import "fmt"

func Arity(f LispType) int {
    if bf, ok := (f).(*LispBuiltinFunction); ok {
        return bf.arity
    } else if uf, ok := (f).(*LispUserDefinedFunction); ok {
        return len(uf.argnames)
    } else {
        panic(fmt.Sprintf("Argument must be a function (got %s instead)", f.repr()))
    }
}

// "parse" a list of evaluated arguments, i.e. split them up in regular args,
// rest args, and keyword args
func ParseFunctionArgs(arity int, args []LispType) *FunctionArgs {
    fa := &FunctionArgs{args: []LispType{}, rest_args: []LispType{},
                        keyword_args: map[string]LispType{}}
    for i := 0; i < len(args); i++ {
        if kw, ok := (args[i]).(*LispKeyword); ok {
            // keyword always must have a value, so it cannot be the last 
            // argument in the list
            if i == len(args)+1 {
                panic("keyword must have a value")
            }
            // keyword should not exist already
            _, exists := fa.keyword_args[kw.value]
            if exists {
                panic(fmt.Sprintf("keyword used multiple times (:%s)", kw.value))
            }
            fa.keyword_args[kw.value] = args[i+1]
            i++
        } else if len(fa.args) >= arity {
            fa.rest_args = append(fa.rest_args, args[i])
        } else {
            fa.args = append(fa.args, args[i])
        }
    }
    // TODO: check if we have enough keywords for arity? or should this be
    // done by the caller?
    return fa
}

// get all "regular" (non-keyword) args from a FunctionArgs struct
func GetAllArgs(fargs *FunctionArgs) []LispType {
    all_args := fargs.args
    all_args = append(all_args, fargs.rest_args...)
    return all_args
}
