package main

import "testing"

func TestReprSymbol(t *testing.T) {
    var sym = NewSymbol("foo")
    AssertEquals(t, "foo", sym.repr())
}

func TestReprEmptyList(t *testing.T) {
    AssertEquals(t, EMPTY_LIST.repr(), "()")
}

func TestReprDottedPair(t *testing.T) {
    var p = &LispPair{NewSymbol("a"), NewSymbol("b")}
    AssertEquals(t, p.repr(), "(a . b)")
    var p2 = &LispPair{NewSymbol("foo"), p}
    AssertEquals(t, p2.repr(), "(foo a . b)")
}

func TestReprProperList(t *testing.T) {
    var p = &LispPair{NewSymbol("q"), &EMPTY_LIST}
    AssertEquals(t, p.repr(), "(q)")
    var p2 = &LispPair{NewSymbol("xyzzy"), p}
    AssertEquals(t, p2.repr(), "(xyzzy q)")
    var p3 = &LispPair{p2, p2}
    AssertEquals(t, p3.repr(), "((xyzzy q) xyzzy q)")
    var p4 = &LispPair{&EMPTY_LIST, p3}
    AssertEquals(t, p4.repr(), "(() (xyzzy q) xyzzy q)")
}

func TestFromSlice(t *testing.T) {
    a := []LispType{NewSymbol("a"), &LispInteger{2}, &TRUE, &EMPTY_LIST}
    b := FromSlice(a)
    AssertEquals(t, b.repr(), "(a 2 true ())")

    a = []LispType{}
    b = FromSlice(a)
    AssertEquals(t, b.repr(), "()")
}

func TestToSlice(t *testing.T) {
    a, _ := tokenize_and_parse("(a 1 b 2)")
    if list, ok := a.(LispList); ok {
        b := list.ToSlice()
        AssertEquals(t, len(b), 4)
        AssertEquals(t, b, []LispType{NewSymbol("a"), &LispInteger{1}, 
                                      NewSymbol("b"), &LispInteger{2}})
    } else {
        t.Error("ToSlice() did not produce a list")
    }
}

func TestCreateFromToken(t *testing.T) {
    a := CreateFromToken("33")
    AssertEquals(t, TypeName(a), "*main.LispInteger")
    b := CreateFromToken("foo")
    AssertEquals(t, TypeName(b), "*main.LispSymbol")
}

