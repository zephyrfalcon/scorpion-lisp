package main

/* Put tests related to TCO here. */

import "testing"

func TestInterpreterTCO(t *testing.T) {
    src := `
      (define (count-down n)
        (if (= n 0) 
          true
          (count-down (+ n -1))))
    `
    intp := NewInterpreter()
    intp.EvalString(src)
    
    intp.call_stack.max_stack_depth = 1000 // the default
    results := intp.EvalString("(count-down 10)")
    AssertEquals(t, results, []LispType{&TRUE})

    // these should fail with a stack overflow if no TCO is applied
    intp.call_stack.max_stack_depth = 5
    results = intp.EvalString("(count-down 10)")
    results = intp.EvalString("(count-down 2000)")

    // TODO: add function that is NOT tail-recursive and make sure it blows up
    // when we run it with a value that's too high!
}

func TestInterpreterTCO_If(t *testing.T) {
    intp := NewInterpreter()
    src := "(if (= 1 1) (+ 2 2) (+ 3 3))"
    expr, _ := tokenize_and_parse(src)
    AssertEquals(t, src, expr.repr())
    sf := NewStackFrame(expr, intp.global_env)
    intp.call_stack.Push(sf)

    okidoki := false
    for i :=0; i < 30; i++ {
        // there should be a point, where we only have ONE frame which has
        // `to be evaluated` == (+ 2 2) 
        // (also as the original expression, actually)
        // this indicates that the `if` has been replaced with one of its
        // condition expressions, as should happen when using TCO.
        if intp.call_stack.Size() == 1 {
            top := intp.call_stack.Top()
            if top.original_expr.repr() == "(+ 2 2)" { 
                okidoki = true
                break 
            }
        }
        //intp.call_stack.Print()
        intp.EvalStep()
    }

    AssertEquals(t, okidoki, true)
}

