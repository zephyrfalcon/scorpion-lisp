package main

import (
    "bufio"
    "flag"
    "fmt"
    "io"
    "os"
    "path/filepath"
    "strings"
    "testing"
)

type LispTestCase struct {
    filename string  // source
    code []string  // code to be evaluated
    expected_result string  // repr of expected result
    lineno int  // line in file that "=>" result is on
}

func CollectTestsInFile(filename string) []LispTestCase {
    testcases := []LispTestCase{}
    current_code := []string{}
    f, _ := os.Open(filename)
    reader := bufio.NewReader(f)
    lineno := 0
    for {
        bytes, _, err := reader.ReadLine()
        lineno += 1
        if err == io.EOF { break }
        line := string(bytes)
        //fmt.Println(line)
        if strings.HasPrefix(line, "=>") {
            testcase := LispTestCase{filename: filename, code: current_code, 
                                     expected_result: strings.TrimSpace(line[2:]),
                                     lineno: lineno}
            testcases = append(testcases, testcase)
            current_code = []string{}
        } else {
            current_code = append(current_code, line)
        }
    }
    return testcases
}

func FindTestFiles() []string {
    flag.Parse()
    root, _ := filepath.Abs(flag.Arg(0))
    stuff, _ := filepath.Glob(root + "/tests/*.test")
    return stuff
}

func RunLispTestCase(t *testing.T, testcase LispTestCase) {
    intp := NewInterpreter()
    code := strings.Join(testcase.code, "\n")
    results := intp.EvalString(code)
    AssertEquals(t, results[len(results)-1].repr(), testcase.expected_result, code)
}

func TestLisp(t *testing.T) {
    all_testcases := []LispTestCase{}
    testfiles := FindTestFiles()
    for _, filename := range testfiles {
        testcases := CollectTestsInFile(filename)
        fmt.Printf("Yay, found %d testcases in %s\n", len(testcases), filename)
        all_testcases = append(all_testcases, testcases...)
    }
    fmt.Printf("Total number of tests: %d\n", len(all_testcases))
    for i, testcase := range all_testcases {
        RunLispTestCase(t, testcase)
        fmt.Print(i+1, " ")
    }
    fmt.Print("tested\n")
}

