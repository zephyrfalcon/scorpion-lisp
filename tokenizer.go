package main

import (
    "fmt"
    "unicode"
)

func tokenize(text string) []string {
    var runes = ([]rune)(text)   
    var tokens []string = make([]string, 0)
    current_token := ""
    in_comment := false
    in_string := false

    for i := 0; i < len(runes); i++ {
        rune := runes[i]
        if in_comment {
            if rune == '\n' {
                in_comment = false
            } else {
                // do nothing, still in comment
            }
        } else if in_string {
            // TODO: handle \" in string literals
            //if rune == '\\' && runes[i+1] == '"' {
            //    // even this isn't correct, what if the string ends in
            //    // "...\\"? then it will mistakenly think this is \" :(
            //}
            if rune == '"' {
                current_token += string(rune)
                tokens = append(tokens, current_token)
                current_token = ""
                in_string = false
            } else {
                current_token += string(rune)
            }
        } else if rune == '(' {
            if len(current_token) > 0 {
                tokens = append(tokens, current_token)
                current_token = ""
            }
            tokens = append(tokens, "(")
        } else if rune == ')' {
            if len(current_token) > 0 {
                tokens = append(tokens, current_token)
                current_token = ""
            }
            tokens = append(tokens, ")")
        } else if unicode.IsSpace(rune) {
            if len(current_token) > 0 {
                tokens = append(tokens, current_token)
                current_token = ""
            }
            // ignore whitespace
        } else if rune == ';' {
            in_comment = true
            if len(current_token) > 0 {
                tokens = append(tokens, current_token)
                current_token = ""
            }
        } else if rune == '\'' {
            if len(current_token) > 0 {
                tokens = append(tokens, current_token)
                current_token = ""
            }
            tokens = append(tokens, "'")
        } else if rune == '"' {
            in_string = true
            current_token += string(rune)
        } else if current_token == "" && rune == '#' {  
            if runes[i+1] == '\\' {
                // character literal
                char_token := string(runes[i:i+3])
                tokens = append(tokens, char_token)
                i += 2  // skip past character literal
                // XXX can we have more than one character? e.g. CL and Scheme
                // allow constructs like #\space, etc. maybe we should try to
                // match until the next whitespace | parenthesis | EOF?
            } else {
                panic(fmt.Sprintf("Unknown #-code: %s...", string(runes[i:i+10])))
            }
        } else {
            current_token += string(rune)
        }
    }

    // if there is still a token being processed, add that
    if len(current_token) > 0 {
        tokens = append(tokens, current_token)
    }

    return tokens
}


