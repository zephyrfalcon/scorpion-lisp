package main

/* Parse a slice of tokens and return the first Lisp expression we can get
 * from it, together with a slice of tokens yet to be processed (if any). */
// TODO: dotted pairs (a . b) currently don't get special treatment.
func parse(tokens []string) (LispType, []string, *Error) {
    if len(tokens) == 0 { return nil, nil, NewError(ERR_NO_INPUT) }  

    if tokens[0] == "(" {
        var list LispList = &EMPTY_LIST
        tokens = tokens[1:]
        for {
            if len(tokens) == 0 {
                return nil, tokens, NewError(ERR_UNBALANCED_PAREN)
            }
            if tokens[0] == ")" {
                // matching closing parenthesis reached
                return list.reverse(), tokens[1:], nil
                // we need to call reverse() because we are constructing the
                // list in the opposite order
            } else {
                var stuff LispType
                stuff, tokens, _ = parse(tokens)
                list = &LispPair{stuff, list}
            }
        }
    } else if tokens[0] == ")" {
        return nil, tokens, NewError(ERR_UNBALANCED_PAREN)
    } else if tokens[0] == "'" {
        // an apostrophe is a reader macro for QUOTE; we read the next
        // expression and return the form (QUOTE <expr>)
        if len(tokens[1:]) == 0 {
            return nil, nil, NewError(ERR_INCOMPLETE_EXPR)
        }
        next, rest, err := parse(tokens[1:])
        if err != nil { 
            return nil, tokens[1:], err 
        }
        return &LispPair{NewSymbol("quote"), &LispPair{next, &EMPTY_LIST}}, rest, nil
    } else {
        // this is an atomic object; return it
        value := CreateFromToken(tokens[0])
        return value, tokens[1:], nil
    }
}


