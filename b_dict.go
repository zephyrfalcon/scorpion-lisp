package main

import "fmt"

// (DICT x)
// Convert a value to a dictionary.
// Currently only accepts a list (k1 v1 k2 v2 ...). May accept other values in
// the future.
// (NOTE: DICT is a converter, so it takes exactly one value. Don't make it
// work with no arguments; different constructors might accept that instead.)
func b_dict(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    if lst, ok := (fargs.args[0]).(LispList); ok {
        values := lst.ToSlice()
        d := NewDictionary()
        for len(values) >= 2 {
            key := values[0]
            value := values[1]
            d.Set(key, value)
            values = values[2:]
        }
        if len(values) == 1 {
            panic(fmt.Sprintf("DICT: Trailing value: %s", values[0].repr()))
        }
        return d, nil
    } else {
        panic("DICT: Argument must be a list")
    }
}

// (DICT-SET! dict key value)
func b_dict_set(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    if dict, ok := (fargs.args[0]).(*LispDictionary); ok {
        dict.Set(fargs.args[1], fargs.args[2])
        return dict, nil
    } else {
        panic("DICT-SET: First argument must be dictionary")
    }
}

// (DICT-GET dict key [default])
func b_dict_get(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    if dict, ok := (fargs.args[0]).(*LispDictionary); ok {
        if len(fargs.rest_args) > 0 {
            value := dict.GetDefault(fargs.args[1], fargs.rest_args[0])
            return value, nil
        } else {
            value, ok := dict.Get(fargs.args[1])
            if ok {
                return value, nil
            } else {
                panic(fmt.Sprintf("DICT-GET: Key not found: %s", 
                      fargs.args[1].repr()))
            }
        }
    } else {
        panic("DICT-GET: First argument must be dictionary")
    }
}

// (DICT-KEYS dict)
func b_dict_keys(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    if dict, ok := (fargs.args[0]).(*LispDictionary); ok {
        keys := dict.Keys()
        return FromSlice(keys), nil
    } else {
        panic("DICT-KEYS: First argument must be dictionary")
    }
}


