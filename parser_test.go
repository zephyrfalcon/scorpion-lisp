package main

import "testing"

// helper function
func tokenize_and_parse(text string) (LispType, []string) {
    tokens := tokenize(text)
    stuff, leftovers, _ := parse(tokens)
    return stuff, leftovers
}

func TestParser(t *testing.T) {
    var thing LispType
    var rest_tokens []string

    thing, rest_tokens = tokenize_and_parse("212")
    AssertEquals(t, thing.repr(), "212")
    AssertEquals(t, rest_tokens, []string{})
    // TODO test if this is an LispInteger rather than an LispSymbol!
    AssertType(t, thing, "*main.LispInteger")

    thing, rest_tokens = tokenize_and_parse("()")
    AssertEquals(t, thing, &EMPTY_LIST)
    AssertEquals(t, thing.repr(), "()")
    AssertType(t, thing, "*main.LispEmptyList")
    AssertEquals(t, rest_tokens, []string{})

    thing, rest_tokens = tokenize_and_parse("foo bar")
    AssertEquals(t, thing.repr(), "foo")
    AssertEquals(t, thing, NewSymbol("foo"))
    AssertType(t, thing, "*main.LispSymbol")
    AssertEquals(t, rest_tokens, []string{"bar"})

    thing, rest_tokens = tokenize_and_parse("(a b c)")
    AssertEquals(t, thing.repr(), "(a b c)")
    AssertEquals(t, thing, &LispPair{NewSymbol("a"), &LispPair{NewSymbol("b"), 
                           &LispPair{NewSymbol("c"), &EMPTY_LIST}}})
    AssertType(t, thing, "*main.LispPair")
    AssertEquals(t, rest_tokens, []string{})

    thing, rest_tokens = tokenize_and_parse("((a (b) c () (d (e (f g))))) bogus")
    AssertEquals(t, thing.repr(), "((a (b) c () (d (e (f g)))))")
    AssertEquals(t, rest_tokens, []string{"bogus"})
}

func TestParserQuote(t *testing.T) {
    thing, rest := tokenize_and_parse("'foo")
    AssertEquals(t, thing.repr(), "(quote foo)")
    AssertEquals(t, rest, []string{})

    thing, rest = tokenize_and_parse("'(1 2 3) 4")
    AssertEquals(t, thing.repr(), "(quote (1 2 3))")
    AssertEquals(t, rest, []string{"4"})
}
