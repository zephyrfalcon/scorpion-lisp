package main

import "testing"

func TestNewLispEnvironment(t *testing.T) {
    env := NewEnvironment()
    AssertEquals(t, env.parent == nil, true)
    AssertEquals(t, len(env.names), 0)

    env2 := NewEnvironmentWithParent(env)
    AssertEquals(t, env2.parent == env, true)
}

func TestLispEnvironmentSimple(t *testing.T) {
    env := NewEnvironment()
    env.Set("foo", &LispInteger{42})
    env.Set("bar", NewSymbol("bar"))
    AssertEquals(t, len(env.names), 2)

    env_found, value, ok := env.Find("foo")
    AssertEquals(t, env_found, env)
    AssertEquals(t, ok, true)
    AssertEquals(t, value, &LispInteger{42})

    value, ok = env.Get("foo")
    AssertEquals(t, ok, true)
    AssertEquals(t, value, &LispInteger{42})

    value, ok = env.Get("xyzzy")
    AssertEquals(t, ok, false)
    
    ok = env.Update("foo", &LispInteger{43})
    AssertEquals(t, ok, true)
    value, ok = env.Get("foo")
    AssertEquals(t, ok, true)
    AssertEquals(t, value, &LispInteger{43})

    ok = env.Update("bogus", &EMPTY_LIST)
    AssertEquals(t, ok, false)

    // TODO: Delete, etc
}

func TestLispEnvironmentWithParent(t *testing.T) {
    parent := NewEnvironment()
    parent.Set("foo", &LispInteger{42})
    parent.Set("bar", NewSymbol("q"))

    child := NewEnvironmentWithParent(parent)
    child.Set("foo", &LispInteger{100})
    child.Set("baz", NewSymbol("z"))

    env, value, ok := child.Find("foo")
    AssertEquals(t, env, child)
    AssertEquals(t, value, &LispInteger{100})
    AssertEquals(t, ok, true)

    env, value, ok = child.Find("bar")
    AssertEquals(t, env, parent)
    AssertEquals(t, value, NewSymbol("q"))
    AssertEquals(t, ok, true)

    value, ok = child.Get("foo")
    AssertEquals(t, value, &LispInteger{100})
    AssertEquals(t, ok, true)

    value, ok = child.Get("bar")
    AssertEquals(t, value, NewSymbol("q"))
    AssertEquals(t, ok, true)

    value, ok = parent.Get("baz")
    AssertEquals(t, ok, false)

    // update 'foo' (available in child and parent)
    ok = child.Update("foo", &LispInteger{101})
    AssertEquals(t, ok, true)
    value, ok = child.Get("foo")
    AssertEquals(t, ok, true)
    AssertEquals(t, value, &LispInteger{101})

    // update 'bar' (available in parent)
    ok = child.Update("bar", NewSymbol("ugh"))
    AssertEquals(t, ok, true)
    value, ok = child.Get("bar")
    AssertEquals(t, ok, true)
    AssertEquals(t, value, NewSymbol("ugh"))

    value, ok = parent.Get("bar")
    AssertEquals(t, ok, true)
    AssertEquals(t, value, NewSymbol("ugh"))

    // 'bar' should still be set in parent, not in child
    value, ok = child.GetLocal("bar")
    AssertEquals(t, ok, false)
}

