package main

import "testing"

func TestDictionary(t *testing.T) {
    d1 := NewDictionary()
    AssertEquals(t, d1.Length(), 0)

    one := &LispInteger{1}
    d1.Set(NewSymbol("a"), one) 
    AssertEquals(t, d1.Length(), 1)

    value, ok := d1.Get(NewSymbol("a"))
    AssertEquals(t, ok, true)
    AssertEquals(t, value, one)

    two := &LispInteger{2}
    d1.Set(NewSymbol("a"), two)
    AssertEquals(t, d1.Length(), 1)

    keys := d1.Keys()
    AssertEquals(t, len(keys), 1)
    AssertEquals(t, FromSlice(keys).repr(), "(a)")

    d1.Set(NewSymbol("b"), &LispInteger{3})
    d1.Set(&LispInteger{44}, NewSymbol("foo"))
    AssertEquals(t, len(d1.Keys()), 3)

    AssertEquals(t, d1.GetDefault(NewSymbol("b"), &FALSE), &LispInteger{3})
    AssertEquals(t, d1.GetDefault(&LispInteger{44}, &FALSE), NewSymbol("foo"))
    AssertEquals(t, d1.GetDefault(&LispInteger{99}, &FALSE), &FALSE)

    // reuse a key
    k := d1.Keys()[0]
    d1.Set(k, &LispInteger{9})

    items := d1.Items()
    AssertEquals(t, len(items), 3)
}
