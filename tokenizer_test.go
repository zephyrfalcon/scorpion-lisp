package main

import "testing"

func TestTokenize(t *testing.T) {
    var text = "42"
    expected := []string{"42"}
    var result = tokenize(text)
    check_tokens(t, result, expected)
    AssertEquals(t, result, expected)

    text = " (  )  "
    expected = []string{"(", ")"}
    result = tokenize(text)
    check_tokens(t, result, expected)
    AssertEquals(t, result, expected)

    text = "(a (b c) *x* do-this)"
    expected = []string{"(", "a", "(", "b", "c", ")", "*x*", "do-this", ")"}
    result = tokenize(text)
    check_tokens(t, result, expected)
    AssertEquals(t, result, expected)

    text = "'x"
    expected = []string{"'", "x"}
    result = tokenize(text)
    check_tokens(t, result, expected)
    AssertEquals(t, result, expected)
}

func TestTokenizerCharacters(t *testing.T) {
    text := `foo #\B bar`
    result := tokenize(text)
    check_tokens(t, result, []string{"foo", "#\\B", "bar"})
}
