;; autoload.sl
;; These definitions are loaded when the interpreter starts.
;; vim: set lisp

(define (caar x) 
  (car (car x)))

(define (cadr x)
  (car (cdr x)))

(define (cdar x)
  (cdr (car x)))

(define (cddr x)
  (cdr (cdr x)))

;; type definitions
(define (int? x) (eq? (type x) @int))
(define (string? x) (eq? (type x) @string))
(define (empty-list? x) (eq? x ()))
(define null? empty-list?) ;; alias
(define (pair? x) (eq? (type x) @pair))
(define (symbol? x) (eq? (type x) @symbol))
(define (char? x) (eq? (type x) @char))
(define (vector? x) (eq? (type x) @vector))
(define (macro? x) (eq? (type x) @macro))
;; more...

(define (list? x)
  (or (pair? x) (empty-list? x)))

;; preliminary outline for a macro expander.
(define (macro-expand form env)
  (if (pair? form)
      (if (symbol? (first form))
          (let (f (env-lookup env (first form)))
            (if (macro? f)
                (let (new-form (macro-apply f (cdr form)))
                  (macro-expand new-form env))
                form))
          form)
      form))

;; when we do a code walker, special forms need to be handled, well,
;; specially; we cannot just expand them like regular forms.
;; e.g. in (lambda args body...), we should leave 'args' alone, but
;; all expressions in body are fair game.
;; also consider: let, cond, define, etc.

(define when
  (macro (cond body)
    (list 'if cond body false)))
;; assumes body is one expression
