package main

/* 
 * In Python, I would have implemented a hierarchy of exceptions, with custom
 * attributes for extra information if necessary. It would be possible to
 * display the name of the exception class (e.g. DivisionByZeroError). 
 *
 * Thanks to Go's brain-damaged^H^H^Hclever design, I have none of these
 * features, so this is the best I can do for now: 
 * 1) an Error struct with a code and a message 
 * 2) a list of constants that map to unique error codes (integers) 
 * 3) a map of descriptions of these error codes 
 * 4) a NewError() function to create instances of the Error struct, which will 
 *    need to be passed around ad nauseam.
 *
 * (I *could* have a number of separate structs for various kinds of errors,
 * but those would have to be cast to the correct type first before I could
 * inspect them... so never mind that, writing this in Go is enough of a hassle
 * as it is.)
 */

import (
    "fmt"
    "strings"
)

// Note: At this point, the numbers mean nothing, but in the future we might
// well want to group them; e.g. 1xx could be syntax errors, 2xx for file
// errors, etc.
const (
    ERR_NO_INPUT = 1
    ERR_EOF = 2
    ERR_UNBALANCED_PAREN = 3
    ERR_INCOMPLETE_EXPR = 4
    ERR_STACK_OVERFLOW = 5
    ERR_STACK_UNDERFLOW = 6
    ERR_INVALID_TYPE = 7
)

var error_names map[int]string = map[int]string{
    1: "no input",
    2: "end of file",
    3: "unbalanced parenthesis",
    4: "incomplete expression",
    5: "stack overflow",
    6: "stack underflow",
    7: "invalid type",
}

type Error struct {
    message string
    code int
}

func (e Error) Error() string {
    return fmt.Sprintf("Error %d: %s", e.code, e.message)
}

func NewError(code int, messages ...string) *Error {
    message := ""
    if len(messages) == 0 {
        ok := false  // stupid Go decl rules 
        message, ok = error_names[code]
        if !ok { message = "(unknown)" }
    } else {
        message = strings.Join(messages, "\n")
    }
    return &Error{code: code, message: message}
}
