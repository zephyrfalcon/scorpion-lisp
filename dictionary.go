package main

import "fmt"

// Q: Do keys need to be pointers as well? They can only be immutables, so...

func NewDictionary() *LispDictionary {
    return &LispDictionary{values: make(map[LispType]LispType)}
}

func NewDictionaryFromMap(m map[LispType]LispType) *LispDictionary {
    d := NewDictionary()

    // due to special handling of keys, we will need to iterate over the
    // original map, then use Set() to set each of them in the target dict
    for key, value := range m {
        d.Set(key, value)
    }

    return d
}

func NewDictionaryFromStringMap(sm map[string]LispType) *LispDictionary {
    d := NewDictionary()
    for s, value := range sm {
        d.Set(NewSymbol(s), value)
    }
    return d
}

/* NOTE: One of Go's more interesting "features" is that an interface, like
* LispType, accepts both LispInteger and &LispInteger. As a rule, we pass all
* objects around as pointers, so &LispInteger, &LispString, etc. However, this
* does not fly well with map keys. We can easily have, say, multiple
* LispSymbol instances with the same value (but different addresses, since
* they're all different entities). The way things are by default, using them
* as map keys will store *pointers* as keys, allowing multiple keys with the
* same value (because they have different pointers. Clearly, this is not what
* we want. We want to use the *value* as the key.
*
* (Interlude: In Python I would be able to override __eq__ and __hash__ on
* objects... but Go doesn't allow that. It doesn't even have objects. I would
* also be able to write a custom dictionary that takes a custom
* comparison/equality function... but again, Go doesn't allow that. So I have
* to resort to the devilry below. If you know a better way, let me know.)
*
* In short, Lisp objects are always passed around with pointers (&LispInteger
* etc), but what we want in this case is the *value*. Unfortunately we cannot
* dereference pointers from just an LispType object, since technically it
* isn't a pointer (although it uses one under the hood). So I resort to the
* following:
* 
* 1. cast a LispType to a concrete struct, *LispInteger, *LispSymbol, etc
* (notice the pointer)
* 2. we get back a pointer to a concrete struct
* 3. dereference that pointer
* 4. store the value we get as the key.
*
* The problems with this approach are obvious... I need to do this typecast
* for each struct type that can be a key, and in multiple places (like Set and
* Get). And when we retrieve the key again, like in Keys(), we need to turn it
* back into a pointer again. If we don't, and some code takes, say, the first
* element of the list returned by Keys(), and tries to use it again as a
* key... that will fail, because it's a struct, not a pointer to a struct like
* almost everything else.
* Also, the actual code is the same for each type, but AFAICT there's no way
* to make this more general.
*
* I don't like that in some cases, a LispType refers to a pointer to a struct,
* and in others, just to the struct itself... but it can't be helped.
*
* In other words, it's a giant pain.
*
* MAYBE something can be done using reflection... but my attempts so far have
* been unsuccessful.
*/

func (d *LispDictionary) Set(key LispType, value LispType) {
    if key.IsMutable() {
        panic(fmt.Sprintf("Dictionary cannot have mutable keys (%s)", key.repr()))
    } else {
        switch t := key.(type) {
        case *LispSymbol: d.values[*t] = value
        case *LispInteger: d.values[*t] = value
        case *LispString: d.values[*t] = value
        case *LispEmptyList: d.values[*t] = value
        default: panic(fmt.Sprintf("unexpected type: %q", t))
        }
    }
}

func (d *LispDictionary) Get(key LispType) (LispType, bool) {
    var result LispType
    var ok bool
    switch t := key.(type) {
    case *LispSymbol: result, ok = d.values[*t]
    case *LispInteger: result, ok = d.values[*t]
    case *LispString: result, ok = d.values[*t]
    case *LispEmptyList: result, ok = d.values[*t]
    default: panic(fmt.Sprintf("unexpected type: %q", t))
    }
    return result, ok
}

func (d *LispDictionary) GetDefault(key LispType, _default LispType) LispType {
    result, ok := d.Get(key)
    if !ok {
        result = _default
    }
    return result
}

func (d *LispDictionary) Contains(key LispType) bool {
    _, ok := d.Get(key)
    return ok
}

func (d *LispDictionary) Keys() []LispType {
    keys := []LispType{}
    for key, _ := range d.values {
        var lkey LispType
        switch t := key.(type) {
        case LispSymbol: lkey = &t
        case LispInteger: lkey = &t
        case LispString: lkey = &t
        case LispEmptyList: lkey = &t
        default: panic(fmt.Sprintf("unexpected type: %q", t))
        }
        keys = append(keys, lkey)
    }
    return keys
}

func (d *LispDictionary) Values() []LispType {
    values := []LispType{}
    for _, value := range d.values {
        values = append(values, value)
    }
    return values
}

// FIXME: Maybe we can make Items() the default, looping over key/value,
// turning the keys back to pointers-to-structs again, then returning
// everything. Perhaps as a slice of some kind of simple new key/value struct.
// Keys() and Values() can then simply call Items() and keep/return the parts
// they want.

func (d *LispDictionary) Items() []LispType {
    /*
    items := []LispType{}
    for key, value := range d.values {
        p := &LispPair{key, &LispPair{value, &EMPTY_LIST}}
        items = append(items, p)
    }
    return items
    */
    items := []LispType{}
    // this is kinda clumsy; Keys() produces pointer-to-structs, as intended,
    // but then each call to Get() dereferences it again in order to get the
    // value in the map. :-/
    for _, key := range d.Keys() {
        value, _ := d.Get(key)
        p := &LispPair{key, &LispPair{value, &EMPTY_LIST}}
        items = append(items, p)
    }
    return items
}

func (d *LispDictionary) Length() int {
    return len(d.values)
}

// TODO:
// Delete()
// Update(other_dict)
// constructors (must check for IsMutable())
