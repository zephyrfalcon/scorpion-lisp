package main

import "fmt"

type CallStack struct { 
    frames []*StackFrame
    max_stack_depth int
}

func NewCallStack() *CallStack {
    cs := CallStack{}
    cs.max_stack_depth = 1000
    return &cs
}

// XXX should we use pointers for all these?

func (cs *CallStack) Push(sf *StackFrame) {
    if len(cs.frames) > cs.max_stack_depth {
        panic("stack overflow")  // fixme: use error code
    }
    cs.frames = append(cs.frames, sf)
}

func (cs *CallStack) Pop() *StackFrame {
    l := len(cs.frames)
    result := cs.frames[l-1]
    cs.frames = cs.frames[:l-1]
    return result
}

func (cs *CallStack) Top() *StackFrame {
    return cs.frames[len(cs.frames)-1]
}

func (cs *CallStack) IsEmpty() bool {
    return len(cs.frames) == 0
}

// are we done evaluating everything? this implies that there is exactly ONE
// frame on the stack, which is atomic, and has an evaluated value.
func (cs *CallStack) IsDone() bool {
    return len(cs.frames) == 1 && cs.frames[0].is_atomic && cs.frames[0].IsDone()
}

func (cs *CallStack) Clear() {
    cs.frames = []*StackFrame{}
}

func (cs *CallStack) Size() int {
    return len(cs.frames)
}

// collapse the top frame, i.e. pop it, take its final value (there should be only
// one) and add it to the `evaluated` slice of the new top frame. (if there is
// only one frame, replace the frame which a new one consisting only of this
// value, marked as evaluated, so the interpreter knows we are done.)
func (cs *CallStack) Collapse(value LispType) {
    old_top := cs.Pop()
    // if this was the top frame, replace it with a new frame consisting of
    // this value
    if len(cs.frames) == 0 {
        newsf := NewStackFrame(value, old_top.env)
        // what if we get back a list as-is? then it should not be in a
        // compound expression, in this case; therefore is_atomic = true
        newsf.is_atomic = true
        newsf.evaluated = []LispType{value}
        cs.Push(newsf)
    } else {
        top := cs.Top()
        if top.aux_data != nil {
            top.aux_data.Receive(value)
        } else {
            top.evaluated = append(top.evaluated, value)
        }
    }
}

func (cs *CallStack) Print() {
    for i := len(cs.frames)-1; i >= 0; i-- {
        cs.frames[i].Print(i+1)
    }
    fmt.Printf("%%\n")
}

