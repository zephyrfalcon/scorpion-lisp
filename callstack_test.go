package main

import "testing"

func TestCallStackGeneral(t *testing.T) {
    cs := NewCallStack()
    // new call stack should be empty
    AssertEquals(t, len(cs.frames), 0)
    AssertEquals(t, cs.IsEmpty(), true)

    // create a (very simple) StackFrame and push it
    sf := NewStackFrame(NewSymbol("a"), nil)
    cs.Push(sf)
    AssertEquals(t, len(cs.frames), 1)
    AssertEquals(t, cs.IsEmpty(), false)

    top := cs.Top()
    AssertEquals(t, top, sf)

    pop := cs.Pop()
    AssertEquals(t, pop, sf)

    AssertEquals(t, len(cs.frames), 0)
    AssertEquals(t, cs.IsEmpty(), true)
}

func TestCallStackCollapse(t *testing.T) {
    cs := NewCallStack()
    env := NewEnvironment()

    // create stack frame for (f x)
    expr := &LispPair{NewSymbol("f"), &LispPair{NewSymbol("x"), &EMPTY_LIST}}
    sf := NewStackFrame(expr, env)
    cs.Push(sf)

    AssertEquals(t, sf.is_atomic, false)
    AssertEquals(t, sf.to_be_evaluated, []LispType{NewSymbol("f"), NewSymbol("x")})
    AssertEquals(t, sf.evaluated, []LispType{})

    // create another stack frame for f; we remove f from the original stack
    // frame's to_be_evaluated slice
    sf2 := NewStackFrame(sf.to_be_evaluated[0], env)
    sf.to_be_evaluated = sf.to_be_evaluated[1:]
    cs.Push(sf2)

    AssertEquals(t, sf2.is_atomic, true)
    AssertEquals(t, sf2.to_be_evaluated, []LispType{}) // empty because atomic!

    // pretend we evaluated f and insert a bogus value
    sf2.evaluated = append(sf2.evaluated, &LispInteger{3})
    
    AssertEquals(t, sf2.IsDone(), true)

    cs.Collapse(&LispInteger{4}) // ignore original value of 3; Collapse doesn't use it
    
    AssertEquals(t, len(cs.frames), 1)
    top := cs.Top()  // we should not look at sf, it's a copy >:
    AssertEquals(t, top.evaluated, []LispType{&LispInteger{4}})
}

