package main

import "fmt"

/* special forms */

/* NOTE:
   - Some special forms return a value. In that case, the functions below 
     return that value. Otherwise, they return nil.
   - Some of these special form functions are called multiple times (e.g.
     IF), and behave differently depending on what has been evaluated so far.
   - When an sf_* function below needs to evaluate something via the call stack, 
     it should just push the relevant frame, then return nil. The caller will
     continue evaluating as usual if nil is returned.
*/

// NOTE: signature and way of calling these functions might change along the
// way.

// (QUOTE X) just returns X as-is
func sf_quote(intp *Interpreter, env *LispEnvironment, args []LispType) (LispType, *Error) {
    return args[1], nil
}

// DEFINE can be used in two ways:
// (DEFINE <name> <value>) to define a variable
// (DEFINE (<name> ...argnames..) ...body...) to define a function
func sf_define(intp *Interpreter, env *LispEnvironment, args []LispType) (LispType, *Error) {
    // DEFINE goes through two passes:
    // 1. evaluate the value (args[2]) via the call stack
    // 2. assign that value to the given name
    if len(args) < 2 {
        panic("DEFINE must have at least two arguments")
    }

    if _, ok := (args[1]).(*LispSymbol); ok {
        return _sf_define_symbol(intp, env, args)
    } else if _, ok := (args[1]).(*LispPair); ok {
        return _sf_define_function(intp, env, args)
        // XXX we *could* just replace the call to define with a modified call
        // (DEFINE <name> (LAMBDA (...args...) ...body...) ? this would avoid
        // duplication of the code that creates an LispUserDefinedFunction.
    } else {
        panic("DEFINE: use (define name value) or (define (f ...) ...)")
    }

}

func _sf_define_symbol(intp *Interpreter, env *LispEnvironment, args []LispType) (LispType, *Error) {
    top := intp.call_stack.Top()
    if len(top.evaluated) == 0 {
        sf := NewStackFrame(args[2], top.env)
        intp.call_stack.Push(sf)
        return nil, nil  // means, evaluate via call stack mechanism
    } else {
        if sym, ok1 := (args[1]).(*LispSymbol); ok1 {
            env.Set(sym.value, top.evaluated[0])
            return top.evaluated[0], nil
        } else {
            panic("DEFINE needs a symbol as its first argument")
        }
    }
}

func _sf_define_function(intp *Interpreter, env *LispEnvironment, args []LispType) (LispType, *Error) {
    if lst, ok := (args[1]).(*LispPair); ok {
        names_exprs := lst.ToSlice()  // a slice of LispTypes; should all be symbols
        names := LispTypeListAsNames(names_exprs)
        fname, argnames := names[0], names[1:]
        fn := &LispUserDefinedFunction{name: fname, argnames: argnames, 
                                       body: args[2:], env: env}  // XXX is env correct?
        env.Set(fname, fn)
        return fn, nil
    } else {
        panic("DEFINE: Invalid call")
    }
}

// (SET! <name> <value>)
func sf_set(intp *Interpreter, env *LispEnvironment, args []LispType) (LispType, *Error) {
    if sym, ok := (args[1]).(*LispSymbol); ok {
        top := intp.call_stack.Top()
        if len(top.evaluated) == 0 {
            // put the value on the call stack for evaluation
            sf := NewStackFrame(args[2], top.env)
            intp.call_stack.Push(sf)
            return nil, nil
        } else {
            uok := env.Update(sym.value, top.evaluated[0])
            if uok {
                return top.evaluated[0], nil
            } else {
                panic(fmt.Sprintf("SET!: No such variable: %s", sym.value))
            }
        }
    } else {
        panic("SET!: first argument must be a symbol")
    }
}

// IF is different... when we see (IF cond x y) then `cond` needs to go on the
// stack to be evaluated; depending on whether it's true or false, we then put
// `x` or `y` on the stack to be evaluated. (this last part can replace the
// stack frame containing the IF, enabling TCO.)
func sf_if(intp *Interpreter, env *LispEnvironment, args []LispType) (LispType, *Error) {
    top := intp.call_stack.Top()
    if len(top.evaluated) == 0 {
        // evaluate the conditional expression using the normal mechanism
        // this should end up in the stack frame's `evaluated` slice.
        sf := NewStackFrame(args[1], env)
        intp.call_stack.Push(sf)
    } else if len(top.evaluated) == 1 {
        // depending on whether the evaluated conditional expression is
        // considered "true", push the 2nd or 3rd argument to be evaluated
        is_true := TruthValue(top.evaluated[0])
        var next LispType
        if is_true { next = args[2] } else { next = args[3] }
        // TCO: get rid of this stack frame (containing the IF)
        intp.call_stack.Pop() 
        // and replace it with a new one containing the expression we want to evaluate
        sf := NewStackFrame(next, env)          
        intp.call_stack.Push(sf)
    } else {
        panic("problem using IF!")  // should not happen but...
    }
    return nil, nil
}

// (LAMBDA (..args..) ..body..)
func sf_lambda(intp *Interpreter, env *LispEnvironment, args []LispType) (LispType, *Error) {
    f, err := _make_function(env, args, false)
    return f, err
}

func sf_macro(intp *Interpreter, env *LispEnvironment, args []LispType) (LispType, *Error) {
    m, err := _make_function(env, args, true)
    return m, err
}

func _make_function(env *LispEnvironment, args []LispType, is_macro bool) (LispType, *Error) {
    name := "LAMBDA"
    if is_macro { name = "MACRO" }
    argnames := []string{}
    if argexprs, ok := (args[1]).(*LispPair); ok {
        names := argexprs.ToSlice()
        for _, x := range names {
            if sym, ok := x.(*LispSymbol); ok {
                argnames = append(argnames, sym.value)
            } else {
                panic(fmt.Sprintf("%s: arguments must be symbols", name))
            }
        }
    } else if _, ok := (args[1]).(*LispEmptyList); ok {
        // pass; list of arguments is empty
    } else {
        panic(fmt.Sprintf("%s: first argument must be a list of symbols", name))
    }
    body := args[2:]
    if is_macro {
        m := &LispMacro{env:env, body:body, argnames:argnames, name:""}
        return m, nil
    } else {
        f := &LispUserDefinedFunction{env:env, body:body, argnames:argnames, name:""}
        return f, nil
    }
}

// (DO exp1 exp2 ... expN)
// Evaluate the expressions in order, returning the value of the last one.
// Like Scheme's BEGIN or Common Lisp's PROGN.
func sf_do(intp *Interpreter, env *LispEnvironment, args []LispType) (LispType, *Error) {
    if len(args) == 0 {
        panic("DO: empty body is not allowed")
    }
    top := intp.call_stack.Top()
    // we store the results of the evaluated expressions in
    // StackFrame.evaluated, so we know how many have been evaluated so far.
    // last one gets special treatment (TCO)
    if len(top.evaluated) == len(args)-2 {
        // last expression in the DO body to be evaluated
        intp.call_stack.Pop() // remove the DO frame
        sf := NewStackFrame(args[len(args)-1], top.env)
        intp.call_stack.Push(sf)
    } else {
        n := len(top.evaluated) + 1 // index of the next expr to evaluate
        sf := NewStackFrame(args[n], top.env)
        intp.call_stack.Push(sf)
    }
    return nil, nil  // means, evaluate via call stack as usual
}

type LetHelper struct {
    values_in []LispType  // names and values to be processed
    evaluated []LispType  // we store evaluated values here
    names []string        // names to be bound, as Go strings
    env *LispEnvironment      // new environment that the names will be evaluated in
}
func (lh *LetHelper) Receive(x LispType) {
    lh.evaluated = append(lh.evaluated, x)
}

// (LET (..names..) ..body..)
// Unlike Lisp/Scheme's LET, the first parameter is a list whose values are
// alternately names and values to be bound to those names.
// E.g. (LET (a 1 b 2) ...)

/* How LET works internally:
   LET creates a new namespace. The names specified in LET's first argument 
   (a list) will be created in this namespace, one by one, so they can refer 
   to each other once they're defined. (Like Scheme's LET* but unlike its LET.)
   We need to evaluate the values associated with the names; to do this, we create
   an auxiliary data structure to keep track of what has been evaluated so far,
   and to store the new namespace. This is necessary because we will revisiting
   the sf_let function multiple times.
   The slice of expressions (name1 value1 name2 value2 ...) will be stored in
   aux.values_in. Whenever we call sf_let, if this slice is not empty, we take
   its first two elements (a name and an expression), then add the name to
   aux.strings, and put the expression up for evaluation.
   When the resulting value comes back (in top.evaluated), we add this to the
   new namespace (as stored in aux.env).
   When aux.values_in is empty, we evaluate the body in the new namespace, which
   now contains all the names+values specified in LET's first argument.
   (TCO is done here.)
*/
func sf_let(intp *Interpreter, env *LispEnvironment, args []LispType) (LispType, *Error) {
    if list, ok := (args[1]).(LispList); ok {
        top := intp.call_stack.Top()
        
        // if we don't have auxiliary data yet, create it
        if top.aux_data == nil {
            newenv := NewEnvironmentWithParent(env)
            lh := &LetHelper{env: newenv, values_in: list.ToSlice()}
            top.aux_data = lh
        } 

        if lh, ok := (top.aux_data).(*LetHelper); ok {
            // a value came back; add it to our auxiliary data's namespace
            if len(lh.evaluated) > 0 {
                name := lh.names[len(lh.names)-1]
                lh.env.Set(name, lh.evaluated[0])
                lh.evaluated = lh.evaluated[1:]
            }
            // check if there are more names/values to be processed
            if len(lh.values_in) > 0 {
                if sym, ok := (lh.values_in[0]).(*LispSymbol); ok {
                    // get symbol name; add to auxiliary names; set corresponding
                    // value up for evaluation *in new namespace*
                    lh.names = append(lh.names, sym.value)
                    sf := NewStackFrame(lh.values_in[1], lh.env)
                    lh.values_in = lh.values_in[2:]
                    intp.call_stack.Push(sf)
                    return nil, nil  // evaluate this frame
                } else {
                    panic("LET: names must be symbols")
                }
            } else {
                // we're done evaluating the names, now evaluate the body (TCO)
                body := &LispPair{NewSymbol("do"), FromSlice(args[2:])}
                sf := NewStackFrame(body, lh.env)
                intp.call_stack.Pop()  // get rid of LET's frame
                intp.call_stack.Push(sf)  // push new frame with body
                return nil, nil  // evaluate via call stack as usual
            }
        } else { panic("!@#$") }

    } else {
        panic("LET: first argument must be a list")
    }
}

// (AND exprs...)
func sf_and(intp *Interpreter, env *LispEnvironment, args []LispType) (LispType, *Error) {
    // evaluate arguments one by one. results are put in frame.evaluated.
    top := intp.call_stack.Top()
    if len(top.evaluated) == len(args)-1 {
        // all done
        if len(top.evaluated) == 0 {
            return &TRUE, nil  // AND was called with no arguments; true by default
        } else {
            // return last evaluated expression
            return top.evaluated[len(top.evaluated)-1], nil
        }
    } else {
        // inspect previous result, if any; if false, the whole AND expression
        // returns false
        if len(top.evaluated) > 0 {
            if !TruthValue(top.evaluated[len(top.evaluated)-1]) {
                return top.evaluated[len(top.evaluated)-1], nil
            }
        }
        // evaluate next part
        expr := args[len(top.evaluated)+1]
        sf := NewStackFrame(expr, env)
        if len(top.evaluated)+1 == len(args)-1 {
            intp.call_stack.Pop()  // last expression: TCO
        }
        intp.call_stack.Push(sf)
        return nil, nil
    }
}

// (OR exprs...)
// XXX much code duplication here. if only Go had macros... >.>
func sf_or(intp *Interpreter, env *LispEnvironment, args []LispType) (LispType, *Error) {
    // evaluate arguments one by one. results are put in frame.evaluated.
    top := intp.call_stack.Top()
    if len(top.evaluated) == len(args)-1 {
        // all done
        if len(top.evaluated) == 0 {
            return &FALSE, nil  // OR was called with no arguments; false by default
        } else {
            // return last evaluated expression
            return top.evaluated[len(top.evaluated)-1], nil
        }
    } else {
        // inspect previous result, if any; if its "truth value" is true, then
        // we return that value, without evaluating the rest of the OR expression
        if len(top.evaluated) > 0 {
            if TruthValue(top.evaluated[len(top.evaluated)-1]) {
                return top.evaluated[len(top.evaluated)-1], nil
            }
        }
        // evaluate next part
        expr := args[len(top.evaluated)+1]
        sf := NewStackFrame(expr, env)
        if len(top.evaluated)+1 == len(args)-1 {
            intp.call_stack.Pop()  // last expression: TCO
        }
        intp.call_stack.Push(sf)
        return nil, nil
    }
}

// (COND (cond1 expr1s...) (cond2 expr2s...) [(else ...)])
// a COND has a number of sub-expressions of the form (condition
// ..exprs..). whenever we evaluate a condition, the result goes onto
// frame.evaluated. if the condition is true, the corresponding
// expressions are evaluated on the stack, and no other parts of the COND
// are processed.
// if no condition is found that is true, the result is false.
func sf_cond(intp *Interpreter, env *LispEnvironment, args []LispType) (LispType, *Error) {
    top := intp.call_stack.Top()

    // check the last condition that we evaluated (if any)
    if len(top.evaluated) > 0 {
        if TruthValue(top.evaluated[len(top.evaluated)-1]) {
            // this condition is true
            // evaluate the corresponding expression(s) (using TCO)
            stuff := args[len(top.evaluated)]
            if list, ok := stuff.(*LispPair); ok {
                // there can be multiple expressions; wrap them in a DO
                result := &LispPair{NewSymbol("do"), list.tail}
                sf := NewStackFrame(result, env)
                intp.call_stack.Pop()
                intp.call_stack.Push(sf)
                return nil, nil
            } else {
                panic(fmt.Sprintf("COND: invalid condition: %q", stuff))
            }
        }
    }

    // are there still parts left to be processed?
    if len(top.evaluated) == len(args)-1 {
        // everything processed, we didn't find a matching condition
        return &FALSE, nil
    } else {
        // process the next condition
        stuff := args[len(top.evaluated)+1]
        if list, ok := stuff.(*LispPair); ok {
            cond := list.head
            // if condition is a symbol "else", then it's considered true
            if sym, ok := cond.(*LispSymbol); ok {
                if sym.value == "else" {
                    top.evaluated = append(top.evaluated, &TRUE)
                    return nil, nil
                }
            }
            sf := NewStackFrame(cond, env)
            intp.call_stack.Push(sf)
            return nil, nil
        } else {
            panic(fmt.Sprintf("COND: invalid condition: %q", stuff))
        }
    }
}

var special_forms map[string]SpecialFormSig = map[string]SpecialFormSig{
    "and": sf_and,
    "cond": sf_cond,
    "define": sf_define,
    "do": sf_do,
    "if": sf_if,
    "lambda": sf_lambda,
    "let": sf_let,
    "macro": sf_macro,
    "or": sf_or,
    "quote": sf_quote,
    "set!": sf_set,
}

