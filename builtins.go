package main

/* built-in functions here... */

import "fmt"
import "reflect"

// TODO:
// - builtins should/could return error code (since Go has no exceptions)
// - we'll need ways to check for required type of arguments, required number of
//   arguments, etc
// - handling numbers will someday be much more complicated

func b_plus(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    result := 0
    all_args := GetAllArgs(fargs)
    for _, value := range all_args {
        if a, ok := value.(*LispInteger); ok {
            result += a.value
        } else {
            errmsg := fmt.Sprintf("number expected; got %s instead (%s)",
                      TypeName(value), value.repr())
            return nil, NewError(ERR_INVALID_TYPE, errmsg)
        }
    }
    return &LispInteger{result}, nil
}

func b_eval(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    // TCO: replace the current stack frame with one containing the expression
    // inside the EVAL.
    sf := NewStackFrame(fargs.args[0], env)
    intp.call_stack.Pop()  // remove frame containing EVAL
    intp.call_stack.Push(sf)  // push frame containing EVAL's expression
    return nil, nil  // continue evaluating via call stack
}

// (APPLY function arglist)
func b_apply(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    // the second argument has to be a list
    if aargs, ok := (fargs.args[1]).(LispList); ok {
        aargslice := aargs.ToSlice()
        evaluated := []LispType{fargs.args[0]}  // function
        evaluated = append(evaluated, aargslice...)  // arguments
        // TCO: replace stack frame with new one
        sf := NewStackFrame(FromSlice(evaluated), env)  // bogus expression
        sf.evaluated = evaluated
        sf.to_be_evaluated = []LispType{}  // nothing left to be evaluated
        intp.call_stack.Pop()  // remove frame containing APPLY
        intp.call_stack.Push(sf)  // push frame containing new expression
        return nil, nil  // continue evaluating via call stack
    } else {
        panic("APPLY: second argument must be a list")
    }
}

type MapHelper struct {
    values_in []LispType    // evaluated values that are passed to the function
    values_out []LispType   // results returned by the function
}
func (m *MapHelper) Receive(x LispType) {
    m.values_out = append(m.values_out, x)
}

// (MAP <function> <list>)
// XXX this is a mess because it breaks the "push sub-expression on stack, get
// result back in frame.evaluated" mechanism. that works for special forms,
// because they don't use frame.evaluated automagically; but a regular builtin
// function like MAP *does*. but when we evaluate the function calls, we
// *don't* want the results put in MAP's frame.evaluated, since it already
// contains the function and the list.
func b_map(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    if list, ok := (fargs.args[1]).(LispList); ok {
        // convert the list to a slice; note that this might not be optimal
        // when working with very long lists
        top := intp.call_stack.Top()
        if top.aux_data == nil {
            mh := MapHelper{values_in: list.ToSlice()}
            top.aux_data = &mh
        }
        if mh, ok := (top.aux_data).(*MapHelper); ok {
            if len(mh.values_in) > 0 {
                expr := &LispPair{fargs.args[0], &LispPair{fargs.args[1], &EMPTY_LIST}}  // bogus
                sf := NewStackFrame(expr, env)
                sf.to_be_evaluated = []LispType{}
                sf.evaluated = []LispType{fargs.args[0], mh.values_in[0]}
                mh.values_in = mh.values_in[1:]
                intp.call_stack.Push(sf)
                return nil, nil
            } else {
                result := FromSlice(mh.values_out)
                return result, nil
            }
        } else {
            panic("MAP: Incorrect auxiliary data")
        }
    } else {
        panic("MAP: second argument must be a list")
    }
}

func b_cons(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    return &LispPair{fargs.args[0], fargs.args[1]}, nil
}

func b_car(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    if pair, ok := (fargs.args[0]).(*LispPair); ok {
        return pair.head, nil
    } else {
        panic("CAR: argument must be a pair")
    }
}

func b_cdr(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    if pair, ok := (fargs.args[0]).(*LispPair); ok {
        return pair.tail, nil
    } else {
        panic("CDR: argument must be a pair")
    }
}

func b_list(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    all_args := GetAllArgs(fargs)
    return FromSlice(all_args), nil
}

// (= a b)  
// Compares two numbers.
// XXX Follows much the same pattern as b_plus; DRY
func b_math_equal(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    a := fargs.args[0]
    b := fargs.args[1]
    if na, aok := a.(*LispInteger); aok {
        if nb, bok := b.(*LispInteger); bok {
            result := ToBoolean(na.value == nb.value)
            return result, nil
        }
    }
    return &FALSE, nil // FIXME
}

func b_repr(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    return &LispString{fargs.args[0].repr()}, nil
}

func b_type(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    return fargs.args[0].Type(), nil
}

func b_supertype(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    if atype, ok := (fargs.args[0]).(*LispTypeObject); ok {
        return atype.supertype, nil
    } else {
        panic(fmt.Sprintf("SUPERTYPE: Argument must be a type; got %s instead",
            fargs.args[0].repr()))
    }
}

func b_type_name(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    if atype, ok := (fargs.args[0]).(*LispTypeObject); ok {
        return &LispSymbol{atype.name}, nil
    } else {
        panic(fmt.Sprintf("TYPE-NAME: Argument must be a type; got %s instead",
            fargs.args[0].repr()))
    }
}

// XXX this currently does not work as expected, thanks to Go's brain-damaged 
// handling of structs, which are copied everywhere. >.<
func b_address(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    addr := reflect.ValueOf(fargs.args[0]).Pointer()
    return &LispInteger{int(addr)}, nil
}

// (EQ? x x) 
// Returns true if the two values refer to exactly the same object.
// (I.e. pointer equivalence)
func b_eq(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    equal := fargs.args[0] == fargs.args[1]  // should work, these are essentially ptrs
    return ToBoolean(equal), nil
}

// (LENGTH x)
// Return the length of the given object. Currently only works on lists.
// Should be a multimethod later that works on various types.
func b_length(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    if list, ok := (fargs.args[0]).(LispList); ok {
        length := list.Length()
        return &LispInteger{length}, nil
    } else {
        panic("LENGTH: First argument must be list")
    }
}

// (ARITY <function>)
// Return the arity of the given function. Arity is defined as the minimum
// number of arguments that the function requires.
func b_arity(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    arity := Arity(fargs.args[0])
    return &LispInteger{arity}, nil
}

// (PRINT x)
// Preliminary version of PRINT.
// Prints the contents of strings; all other values are printed as-is.
// XXX for now, this is just for testing/debugging; flesh this out later.
func b_print(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    if s, ok := (fargs.args[0]).(*LispString); ok {
        fmt.Println(s.value)
    } else {
        fmt.Println(fargs.args[0].repr())
    }
    return fargs.args[0], nil
}

// (APPEND list1 list2)
// Append two lists, returning a new list.
// (Should be a multimethod later than operates on other types as well.)
func b_append(intp *Interpreter, env *LispEnvironment, fargs *FunctionArgs) (LispType, *Error) {
    if list1, ok := (fargs.args[0]).(LispList); ok {
        if list2, ok := (fargs.args[1]).(LispList); ok {
            elems := list1.ToSlice()
            elems2 := list2.ToSlice()
            elems = append(elems, elems2...)
            return FromSlice(elems), nil
        } else {
            panic("APPEND: Second argument must be a list")
        }
    } else {
        panic("APPEND: First element must be a list")
    }
}

// FunctionInfo, FI for short
type FI struct {
    f BuiltinFunctionSig
    arity int
}

var builtins map[string]FI = map[string]FI{
    "+": FI{b_plus, 0},
    "=": FI{b_math_equal, 2},
    "address": FI{b_address, 1},
    "append": FI{b_append, 2},
    "apply": FI{b_apply, 2},
    "arity": FI{b_arity, 1},
    "car": FI{b_car, 1},
    "cdr": FI{b_cdr, 1},
    "cons": FI{b_cons, 2},
    "eq?": FI{b_eq, 2},
    "eval": FI{b_eval, 1},
    "length": FI{b_length, 1},
    "list": FI{b_list, 0},
    "map": FI{b_map, 2},
    "print": FI{b_print, 1},
    "repr": FI{b_repr, 1},
    "supertype": FI{b_supertype, 1},
    "type": FI{b_type, 1},
    "type-name": FI{b_type_name, 1},

    // b_dict.go (dictionaries)
    "dict": FI{b_dict, 1},
    "dict-get": FI{b_dict_get, 2},
    "dict-set!": FI{b_dict_set, 3},
    "dict-keys": FI{b_dict_keys, 1},
}

