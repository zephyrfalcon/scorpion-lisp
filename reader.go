package main

/*
 * A Reader is an object that reads Lisp expressions from a stream (usually a
 * string or a file). The main method is Read(), which returns the next
 * expression found (if any, otherwise nil), and an error code if something
 * went wrong.
 *
 * Like a file object, a Reader keeps track of what parts of a stream have not
 * been processed yet. In the case of FileReader, it will read from its
 * associated file as necessary until enough tokens have been scanned to form
 * a complete expression.
 *
 * If an incomplete expression has been read, but there is no more input, then
 * this is of course an error, and ERR_UNBALANCED_PAREN will be returned.
 *
 * If a Reader has no more input left, either from tokens already read or from
 * its associated file, then it will return ERR_NO_INPUT. This is usually not
 * an error, but simply indicates that there is nothing left to read.
 *
 * It is probably best to read files whole as one big string, then process
 * them via StringReader, unless we have no other choice for some reason (e.g.
 * in the case of stdin).
 *
 */

import (
    "bufio"
    "fmt"
    "io"
    "os"
)

type Reader interface {
    Read() (LispType, *Error)
}

type FileReader struct {
    file *bufio.Reader
    tokens_read []string
}

type StringReader struct {
    tokens []string
}

/* constructors */

func NewFileReader(file *bufio.Reader) FileReader {
    return FileReader{file: file}
}

func NewFileReaderFromFilename(filename string) FileReader {
    f, _ := os.Open(filename)
    r := bufio.NewReader(f)
    return NewFileReader(r)
}

func NewStringReader(s string) StringReader {
    tokens := tokenize(s)
    return StringReader{tokens}
}

/* methods */

func (fr *FileReader) Read() (LispType, *Error) {
    for {
        // first, try to read expression from tokens we already have
        expr, rest_tokens, err := parse(fr.tokens_read)
        if err == nil {
            fr.tokens_read = rest_tokens
            return expr, nil
        }
        // that didn't work. let's read another line
        line, ferr := fr.file.ReadString('\n')
        if line == "" && ferr == io.EOF {
            if len(fr.tokens_read) > 0 {
                return nil, NewError(ERR_UNBALANCED_PAREN, "Incomplete expression")
            }
            // Q: can there still be stuff in `line` which needs processed?
            return nil, NewError(ERR_NO_INPUT)
        } else if ferr != nil {
            panic(fmt.Sprintf("Unknown error: %w", ferr))
        }
        tokens := tokenize(line)
        fr.tokens_read = append(fr.tokens_read, tokens...)
        // go for another round
    }
}

func (sr *StringReader) Read() (LispType, *Error) {
    expr, rest_tokens, err := parse(sr.tokens)
    if err != nil { return nil, err }
    sr.tokens = rest_tokens
    return expr, nil
}

