package main

import "fmt"

// interface for those built-in functions that need to manage auxiliary data
// on the stack frame. (LET, MAP, etc)
type FrameDataHelper interface {
    Receive(LispType)
}

// StackFrame:
// Used to keep track of evaluating expressions.
// If an expression is atomic (i.e. anything other than a pair), `is_atomic`
// will be true, and the result of evaluating the expression will go into
// `evaluated`.
// If the expression is a pair (assumed to be a proper list), then its
// elements will (normally) be evaluated one by one; the evaluated values will
// go into `evaluated`, while `to_be_evaluated` will shrink until empty.
// EXCEPTION: Special forms, which will each handle evaluation differently;
// there will be separate functions for these.
//
type StackFrame struct {
    original_expr LispType      // the original Lisp expression
    to_be_evaluated []LispType  // yet to be evaluated
    evaluated []LispType        // what we evaluated so far
    is_atomic bool              // is the original expression atomic
    env *LispEnvironment            // LispEnvironment that expression will be evaluated in
    aux_data FrameDataHelper    // auxiliary data, differs per function that needs it
}

func NewStackFrame(expr LispType, env *LispEnvironment) *StackFrame {
    if p, ok := expr.(*LispPair); ok {
        // compound expression (i.e. a list)
        return &StackFrame{original_expr: expr, to_be_evaluated: p.ToSlice(), 
                           evaluated: []LispType{}, is_atomic: false, env: env, 
                           aux_data: nil}
    } else {
        // atomic expression (anything else)
        return &StackFrame{original_expr: expr, to_be_evaluated: []LispType{}, 
                           evaluated: []LispType{}, is_atomic: true, env: env, 
                           aux_data: nil}
    }
}

// return true if we haven't evaluated anything yet. this is important when
// evaluating pairs (lists), since the first element of the list is the
// operator (or it might be the name of a special form)
func (sf *StackFrame) IsUnevaluated() bool {
    return len(sf.evaluated) == 0
}

// are we done evaluating all the parts of the expression in this stack frame?
func (sf *StackFrame) IsDone() bool {
    return ((!sf.is_atomic && len(sf.to_be_evaluated) == 0) || 
            (sf.is_atomic && len(sf.evaluated) == 1))
}

// print the contents of the stack frame. the `no` indicates the position in
// the call stack.
func (sf *StackFrame) Print(no int) {
    fmt.Printf("---frame %d---\n", no)
    fmt.Printf("  %s -- %s\n", Choose(sf.is_atomic, "atomic", "compound"), 
                               Choose(sf.IsDone(), "DONE", "not done"))
    fmt.Printf("  expression: %s\n", sf.original_expr.repr())
    fmt.Printf("  evaluated: %q\n", LispTypeListAsReprs(sf.evaluated))
    fmt.Printf("  to be evaluated: %q\n", LispTypeListAsReprs(sf.to_be_evaluated))
}

