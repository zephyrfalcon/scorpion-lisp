package main

import "testing"

// General tests for the Interpreter object.

func TestInterpreter(t *testing.T) {
    intp := Interpreter{}
    intp.Init()

    // simple evaluation (integer literal)
    expr := &LispInteger{42}
    result := intp.EvalExpr(expr, intp.global_env)
    AssertEquals(t, result, expr)
}

func TestInterpreterLookupNames(t *testing.T) {
    intp := NewInterpreter()
    // create a name 'foo' in the global environment
    intp.global_env.Set("foo", &LispInteger{42})
    // evaluate the expression "foo"
    expr := NewSymbol("foo")
    result := intp.EvalExpr(expr, intp.global_env)
    AssertEquals(t, result, &LispInteger{42})
}

func TestCallFunction(t *testing.T) {
    intp := NewInterpreter()
    args := []LispType{&LispInteger{3}, &LispInteger{4}}
    fargs := &FunctionArgs{args: args}
    callable, _ := intp.builtin_env.Get("+")
    result := CallFunction(&intp, intp.global_env, callable, fargs)
    AssertEquals(t, result, &LispInteger{7})
}

func TestInterpreterEvalCompoundExpr(t *testing.T) {
    intp := NewInterpreter()
    expr := &LispPair{NewSymbol("+"), &LispPair{&LispInteger{1}, &LispPair{&LispInteger{2}, &EMPTY_LIST}}}
    result := intp.EvalExpr(expr, intp.global_env)
    AssertEquals(t, result, &LispInteger{3})

    results := intp.EvalString("(+ 3 4)")
    AssertEquals(t, results, []LispType{&LispInteger{7}})

    results = intp.EvalString("(+ (+ 1 2) (+ 3 4))")
    AssertEquals(t, results, []LispType{&LispInteger{10}})

    results = intp.EvalString("1 2")
    AssertEquals(t, results, []LispType{&LispInteger{1}, &LispInteger{2}})
}

func TestInterpreterQuote(t *testing.T) {
    intp := NewInterpreter()

    results := intp.EvalString("(quote x)")
    AssertEquals(t, results, []LispType{NewSymbol("x")})
}

func TestInterpreterDefine(t *testing.T) {
    intp := NewInterpreter()

    results := intp.EvalString("(define foo 42)")
    AssertEquals(t, results, []LispType{&LispInteger{42}})
    results = intp.EvalString("foo")
    AssertEquals(t, results, []LispType{&LispInteger{42}})
    results = intp.EvalString("(+ foo 1)")
    AssertEquals(t, results, []LispType{&LispInteger{43}})

    value, ok := intp.global_env.Get("foo")
    AssertEquals(t, ok, true)
    AssertEquals(t, value, &LispInteger{42})

    results = intp.EvalString("(define bar (+ 1 2))")
    AssertEquals(t, results, []LispType{&LispInteger{3}})
}

func TestInterpreterIf(t *testing.T) {
    intp := NewInterpreter()

    results := intp.EvalString("(if true 1 2)")
    AssertEquals(t, results, []LispType{&LispInteger{1}})
    results = intp.EvalString("(if false 1 2)")
    AssertEquals(t, results, []LispType{&LispInteger{2}})
    results = intp.EvalString("(if true (+ 1 2) (+ 3 4))")
    AssertEquals(t, results, []LispType{&LispInteger{3}})
    // TODO: make sure other expression inside the IF is not called!
}

func TestInterpreterLambda(t *testing.T) {
    intp := NewInterpreter()

    results := intp.EvalString("(lambda (x) x)")
    AssertEquals(t, TypeName(results[0]), "*main.LispUserDefinedFunction")
    if f, ok := (results[0]).(*LispUserDefinedFunction); ok {
        AssertEquals(t, f.name, "")
        AssertEquals(t, f.body, []LispType{NewSymbol("x")})
        AssertEquals(t, f.argnames, []string{"x"})
        AssertEquals(t, f.env, intp.global_env)
    } else {
        t.Error("expected a lambda")
    }

    // try again with another lambda...
    results = intp.EvalString("(define inc (lambda (x) (+ x 1)))")
    results = intp.EvalString("(inc 3)")
    AssertEquals(t, results, []LispType{&LispInteger{4}})

    // call the lambda directly in the first position
    results = intp.EvalString("((lambda () 5))")
    AssertEquals(t, results, []LispType{&LispInteger{5}})

    // TODO: call a lambda that returns a lambda, then call *that* lambda

    // TODO: check environments

    // test lambda with multiple expressions in body
    // (should expand to a DO expression)
}

func TestInterpreterDo(t *testing.T) {
    intp := NewInterpreter()

    results := intp.EvalString("(do 1 2 3)")
    AssertEquals(t, results, []LispType{&LispInteger{3}})

    results = intp.EvalString("(do 1 2 (define x 5) 3)")
    AssertEquals(t, results, []LispType{&LispInteger{3}})
    value, ok := intp.global_env.Get("x")
    AssertEquals(t, ok, true)
    AssertEquals(t, value, &LispInteger{5})
}

func TestInterpreterFunctionArgs(t *testing.T) {
    // (a b c :foo 1 :bar 2 3)
    list := []LispType{NewSymbol("a"), NewSymbol("b"), NewSymbol("c"),
                       NewKeyword("foo"), &LispInteger{1}, 
                       NewKeyword("bar"), &LispInteger{2},
                       &LispInteger{3}}
    fa := ParseFunctionArgs(2, list)
    AssertEquals(t, fa.args, []LispType{NewSymbol("a"), NewSymbol("b")})
    AssertEquals(t, fa.rest_args, []LispType{NewSymbol("c"), &LispInteger{3}})
    AssertEquals(t, len(fa.keyword_args), 2)
    foo, _ := fa.keyword_args["foo"]
    AssertEquals(t, foo, &LispInteger{1})
    bar, _ := fa.keyword_args["bar"]
    AssertEquals(t, bar, &LispInteger{2})

    // XXX add more tests...
}

