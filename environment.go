package main

/* constructors */

// XXX could be rolled into one?
func NewEnvironment() *LispEnvironment {
    return &LispEnvironment{nil, make(map[string]LispType)}
}

func NewEnvironmentWithParent(parent *LispEnvironment) *LispEnvironment {
    return &LispEnvironment{parent, make(map[string]LispType)}
}

/* methods */

func (e *LispEnvironment) Set(name string, value LispType) {
    e.names[name] = value
}

// find a name in the environment. if not found, search the parent environment
// (if any) recursively. 
// returns a 3-tuple consisting of the LispEnvironment where it was found, the
// value associated with the name, and a boolean indicating success (found) or
// not.
func (e *LispEnvironment) Find(name string) (*LispEnvironment, LispType, bool) {
    value, ok := e.names[name]
    if ok {
        return e, value, ok
    } else {
        if e.parent != nil {
            return e.parent.Find(name)
        } else {
            return e, nil, false
        }
    }
}

func (e *LispEnvironment) Get(name string) (LispType, bool) {
    _, value, ok := e.Find(name)
    return value, ok
}

func (e *LispEnvironment) GetLocal(name string) (LispType, bool) {
    value, ok := e.names[name]
    return value, ok
}

// update a value associated with the given name, i.e. find it in the
// environment *or a parent*, then update the value in *that environment*.
// returns true if the name was found and could be updated, false otherwise.
func (e *LispEnvironment) Update(name string, value LispType) bool {
    env_found, _, ok := e.Find(name)
    if ok {
        env_found.Set(name, value)
        return true
    } else {
        return false
    }
}

// delete a name from this environment. do not look in parents.
func (e *LispEnvironment) DeleteLocal(name string) {
    delete(e.names, name)
}

// look for a name in this environment and parents, then delete it when found.
// NOTE: only deletes the first instance found.
func (e *LispEnvironment) Delete(name string) {
    env_found, _, ok := e.Find(name)
    if ok {
        env_found.DeleteLocal(name)
    }
}

// TODO:
// Keys() (both local and with all parents)
// Slice of (key, value) pairs
// HasKey(name)

