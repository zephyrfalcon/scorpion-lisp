# Scorpion Lisp types -- some thoughts

(2017-01-16)

As I currently envision it, Scorpion Lisp will have three major groups of
types:

1. built-in types
2. structs
3. algebraic types

## Idea: types are objects themselves

Much like in Python, types could be objects themselves. So `int` is not just a
symbol, it's a type object, which can be used to create new instances of that
type, or convert instances of a different type.

In other words, `int` &c would be both constructors and converters. They are
also callables, which might make things hairy, since they're not just
run-of-the-mill functions.

The idea is, that if you ask for an object's type, you get a type object back:

```
(type 3)
=> int
(type "hello")
=> string
(type '(1 2 3))
=> pair
```

(Note that there is *no* `list` type. What we do have is `pair` (aka "cons" in
Common Lisp) and `empty-list`.)

(Also note that the actual representations might vary. I.e. instead of
printing just "int", we could show something like `#int` or #(type int) or
`<int>` or whatever.)

Types are callable and can be used to create new objects or convert them:

```
(int "4")
=> 4
(string 44)
=> "44"
(string 'xyz)
=> "xyz"
```

This implies that types are also multimethods. It makes sense if we can define
our own versions, for our own types.

We can check if an object is an instance of a certain type with
`instance-of?`:

```
(instance-of? 3 int)
=> true
```

For each built-in type, and for each user-defined type, there will be a
function `foo?` that checks if an object is of the given type:

```
(int? 3)
(int? "xyz")
(string? 3)
```

```
(define-struct point x y z)
(define p (make-point 1 2 3))   ;; yet to be decided...
(point? p)
=> true
(point? 1)
=> false
```

The question is, if this all will play well together. Type objects would have
multiple functions. They would also effectively be multimethods (and it should
be possible to extend them arbitrarily). 

Not all type objects are the preferred way to create objects of their type.
Since they should all take exactly one parameter, we cannot do this:

```
(vector 1 2 3)  ;; wrong
(pair 1 2)      ;; wrong
```

That is, `vector` will essentially be useless a constructor, although it would
still be useful as a converter: `(vector '(1 2 3))` etc. If we want a function
that creates a new vector like `list` creates a list, we have to add one with
a different name. `vector*` perhaps.
The same is true for `pair`. It seems like `cons` would fill that function, or
perhaps a new function `pair*` which would take an arbitrary number of
arguments:

```
(pair* 1 2)      => (1 . 2)
(pair* 1 2 ())   => (1 2)
(pair* 1 2 3 4)  => (1 2 3 . 4)
;; but:
(pair*)          => error
(pair* 1)        => error
```

Some other constructors would make no sense, like `empty-list` or `bool`. The
latter can be used to get the "truth value" of any expression, though.

It seems like a `type` is itself actually a new type in Scorpion Lisp, which
is callable like a function and redefinable like a multimethod. (But *is* it
technically a multimethod?)

## An alternative

We could separate all these different roles. So `int` could still be a type
object, or it could just be a symbol... although that means all functions that
do something with types, have to work with symbols now instead.

```
(type 3)
=> int
(instance-of? 3 'int)
=> true
(define-struct point3d 'point2d ...)   ;; does not look good
```

Or, we could have a special naming convention for types, like Gauche does. So
`<int>`, etc. This has the problem that you have to follow this convention
carefully when making your own types, *or* Scorpion Lisp can add them on its
own... neither seems like a great solution...

```
(define-struct point x y z)
;; wrong; should be <point>
```

NOTE: Objects seem better than symbols, esp. when we add modules to the
language, and types can be confined to a module. In that case it no longer
works to identify them with just a symbol name. A (singleton) object would
work fine, though.

In any case, we can still separate constructors and converters from the actual
type object. For structs we already have `make-FOO` like in Scheme and CL. We
could automatically create multimethods as well for converters. For example:

* `int` (type name or symbol; not callable)
* `int?` (test if an object is an int)
* `to-int` (convert an object to an int; multimethod)

and:

* `point`
* `point?`
* `to-point`
* `make-point` (for structs)

There seems to be too much of a gap here between built-in types and
user-defined ones (not even counting the algebraic ones).

(As it is, algebraic types might not be even be necessary or useful. Lisp is a
dynamic language and we can just match the "shape" of an expression.)

