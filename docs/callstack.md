# An explanation of the Scorpion Lisp call stack

## Overview

All Scorpion Lisp expressions are evaluated via the call stack. This stack
consists of *stack frames* that contain various bits of information about the
(sub)expression being evaluated.

An *atomic expression* (consisting of an atomic value; basically anything that
isn't a non-empty list) is evaluated directly. If the expression is a symbol,
it will be looked up in the *environment* associated with the stack frame.
Otherwise it is just returned as-is (most atomic expressions evaluate to
themselves).

A *compound expression*, on the other hand, is evaluated differently. This is
a list which one or more elements. This is often a function call, in which
case each part of the list is evaluated separately. Once all parts have been
evaluated, we can call the function with its arguments. The result of that
function call is then returned to the parent stack frame.

That is the normal evaluation of compound expressions, but there are two
exceptions to this. In the case of a *special form*, the unevaluated parts of
the list are passed to a built-in function, which might evaluate some of them
separately. For example, `if` evaluates its first element, then depending on
whether that is considered a "true" value or not, it evaluates the second *or*
the third, and return that. On the other hand, `quote` does not evaluate its
argument at all, but returns it as-is.

The second exception is that certain built-in functions (not special forms)
need to do additional evaluations. For example, `(map f '(1 2 3))` gets the
result of evaluating the name `f` (which should be a function) and the
evaluated list `(1 2 3)`, but it will need to evaluate `(f 1)`, `(f 2)` and
`(f 3)` to construct the resulting list. This is done by manipulating the call
stack directly.

## The evaluation mechanism

A stack frame consists of the following data:

* `original_expr`, the original expression that should be evaluated;
* `env`, the environment that it should be evaluated in;
* `to_be_evaluated`, a Go slice of unevaluated subexpressions (in case of a
  compound expression);
* `evaluated`, a Go slice of evaluated results; this is initially empty;
* `is_atomic`, a boolean indicating whether the original expression is atomic.

A stack frame is considered *done* evaluating if

* it is atomic and `evaluated` holds one element (the result of evaluating the
  original expression); or
* it is not atomic, `to_be_evaluated` is empty, and `evaluated` holds one or
  more evaluated values.

When an *atomic expression* is evaluated, `is_atomic` will be true. In case of
a symbol, a lookup will be done in `env`; otherwise the expression will
evaluate to itself. The result is put in `evaluated` which will hold only one
element in this case.

When a *compound expression* is evaluated, `to_be_evaluated` will consists of
the unevaluated parts of that expression. For example, if we evaluate 
`(f x y)`, then `to_be_evaluated` will be a slice of symbols `f`, `x` and `y`.
To evaluate these, they will be put on the stack one by one. So a new stack
frame is pushed containing `f` (and the right environment), which will then be
evaluated during the next evaluation step. When the result is determined, that
frame is "collapsed", i.e. it is popped and the result is added to the parent
frame; in this case, to our `(f x y)` frame's `evaluated`. This will continue
until all parts have been evaluated. Then, the function indicated by `f` is 
called with the values of `x` and `y`; the result will be collapsed again to
the parent frame, etc.

Naturally, if there is no parent frame, then we are done evaluating, and the
result will be the result of the whole expression.

## Calling user-defined functions

In the description above we glossed over the details of function calls. If the
function is built-in, a corresponding Go function is called, which will
(usually) return a value, which will then be processed as usual.

However, if the function is user-defined, more work needs to be done. A new
environment is created, based on the environment that the function is called
in. In this new environment, the parameter names of the function are bound to
the corresponding values that were passed to the function call. For example,
if we define a function `(foo x y)`, and we call this like `(foo 1 2)`, then a
new environment is created with `x` bound to 1 and `y` bound to 2. The old
stack frame (containing the function call) is then replaced with a new one,
containing this new environment and an expression that is *the user-defined
function's body*. (If the body consists of multiple expressions, then these
are wrapped in a `do` call.)

For example, if we have a function like this:

```
(define (foo x y)
  (print x)
  (+ x y 1))
```

and we call it with `(foo 1 2)`, we get (after evaluating the parts of that
list), a stack frame like this:

```
env:  x=1 y=2
body: (do
        (print x)
        (+ x y 1))
```

...which will then be evaluated as usual.

## Auxiliary data

A `StackFrame` struct has another attribute, `aux_data`, which is only used in
certain cases. 

Some functions and special forms need to evaluate and keep around more data
than the regular `evaluated` and `to_be_evaluated` attributes can handle. In
such cases, `auxiliary` is used (it needs to be created first, normally the
value is nil, since most stack frames don't use it.) 

For example, the special form `LET` evaluates a number of values and binds
them to names, one by one, in a new namespace, then evaluates its body in that
namespace. In order to do this, there is a special struct called `LetHelper`,
which has attributes to store this new namespace, values before and after
evaluation etc. The name/value part of the `LET` is evaluated this way; when
done, the body is evaluated in the new namespace (using the regular mechanism). 

Some built-in functions need to use this mechanism as well, like `MAP`, which
uses `MapHelper` to store the result of values passed to its function. 

**NOTE:** If `aux_data` is nil, then the regular mechanism will be used
(`StackFrame.to_be_evaluated` => `StackFrame.evaluated`), but if it is not
nil, a helper class is assumed to be available, with a `Receive()` method
which will handle how an evaluated value is stored. 
