## Coding Guidelines

*Directions for hacking on the Go source.*

### Use pointers

When using structs, use pointers whenever possible. E.g. don't do this:

```
func Blah() MyStruct {
    s := MyStruct{...}
    s.foo = bar  // do stuff...
    return s
}
```

Rather, use:

```
func Blah() *MyStruct {
    s := &MyStruct{...}
    s.foo = bar  // do stuff... notation stays the same
    return s
}
```

When using interfaces (like with `LispType`, we don't need to use pointers
when referring to the interface type. So a return type of `LispType`, or
`[]LispType`, is fine. But when using concrete structs, use a pointer.

It is easy to forget the `&` operator when creating a new struct. Where
appropriate or useful, create a helper function (like `NewEnvironment`, etc)
that takes some arguments, creates a struct,  fills in reasonable defaults
elsewhere, and returns a pointer to the newly created struct.

### Possible future changes

* It is not clear whether something is an interface or a struct. Structs take
  pointers, while interfaces actually *are* pointers behind the scenes. To
  make the difference clearer, maybe we should start interface names with "I"
  (`ILispType`, `ILispList`, etc). Especially as their number grows, this is
  something to consider.

