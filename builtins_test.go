package main

import "testing"

func TestPlus(t *testing.T) {
    intp := NewInterpreter()
    args := []LispType{&LispInteger{4}, &LispInteger{5}}
    fargs := &FunctionArgs{args: args}
    result, _ := b_plus(&intp, intp.global_env, fargs)
    AssertEquals(t, result, &LispInteger{9})
}

