package main

import "testing"

func TestNewStackFrame(t *testing.T) {
    var expr LispType = NewSymbol("z")
    sf := NewStackFrame(expr, nil)
    AssertEquals(t, sf.is_atomic, true)
    AssertEquals(t, sf.original_expr, expr)
    AssertEquals(t, sf.to_be_evaluated, []LispType{})
    AssertEquals(t, sf.evaluated, []LispType{})

    expr = &LispPair{NewSymbol("y"), &EMPTY_LIST}
    sf = NewStackFrame(expr, nil)
    AssertEquals(t, sf.is_atomic, false)
    AssertEquals(t, sf.original_expr, expr)
    AssertEquals(t, sf.to_be_evaluated, []LispType{NewSymbol("y")})
    AssertEquals(t, sf.evaluated, []LispType{})
}

