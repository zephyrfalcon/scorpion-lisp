;; builtin tests
;; TODO: Need organized and split up over multiple files

(eval 3)  ;; evaluate atomic value
=> 3

(eval (quote (+ 1 2)))
=> 3

(eval (quote (define x 5)))
x
=> 5

(define x 6)
x
=> 6

(define inc (lambda (x) (+ x 1)))
(inc 6)
=> 7

(apply + (quote (5 6)))
=> 11

(define (add-two x) (+ x 2))
(add-two 7)
=> 9

(define (foo x)
  (set! x (+ x 1))
  x)
(foo 4)
=> 5

(define x 10)
(define (foo)
  (define x 20)
  (set! x 21)
  x)
(foo)
x
=> 10

(let () 42)
=> 42

(let (a 1
      b 2)
  (+ a b))
=> 3

(let (a (+ 1 2)
      b (+ a 5)
      c (+ a b))
  (+ a b c))
=> 22

(define (inc x) (+ x 1))
(map inc (quote (1 2 3)))
=> (2 3 4)

(define a (cons 1 ()))
(car a)
=> 1
(define b (cons 3 ()))
(cdr b)
=> ()
(define c (cons 5 (cons 6 (cons 7 ()))))
(cdr c)
=> (6 7)

(and 1 2 3)
=> 3
(and 1 2 false 5)
=> false

;; test side effects
(define x 0)
(define result (and 1 2 (set! x 1) 3))
(list result x)
=> (3 1)

;; test side effects, #2
(define x 0)
(define result (and 1 2 false (set! x 1)))
(list result x)
=> (false 0)

(or 1 2 3)
=> 1
(or)
=> false
(or false true false)
=> true

(define x 0)
(define result (or 1 2 (set! x 1) 3))
(list result x)
=> (1 0)

(define x 0)
(define result (or false (set! x 1) false 4))
(list result x)
=> (1 1)

(cond
  (true 1)
  (false 2))
=> 1

(cond
  (false 99)
  (3 (+ 1 2) (+ 3 4))
  (4 5))
=> 7

(define x 0)
(define result
  (cond
    (3 4)
    (4 (set! x 1))
    (5 (set! x 2))
    (else 99)))
(list result x)
=> (4 0)

;; if all conditions are false, COND returns false as well
(cond
  (false 1)
  (false 2)
  (false 3))
=> false

(cond
  (false 1)
  (false 2)
  (else 9))
=> 9

;; test case sensitivity
(DEFINE x 4)
(deFine Y (+ X 2))
(LIST x y)
=> (4 6)

;; identity

(define x 1)
(= (address x) (address x))
=> true

(= (address +) (address +))
=> true

(eq? 1 1)  
;; currently this is false, since two different LispInteger objects are
;; created; however, this might change in the future (for some values)
=> false
(eq? '(1 2 3) '(1 2 3))
=> false
(define x '(a b c))
(eq? x x)
=> true

(length ())
=> 0
(length '(a b c d))
=> 4

(arity +)
=> 0
(define (foo x y) 42)
(arity foo)
=> 2

;; test %args
(define (my-list) %rest)
(my-list 1 2 3)
=> (1 2 3)

(define (foo x y) (cons x (cons y %rest)))
(foo 4 5 6 7)
=> (4 5 6 7)

;; test %keywords
(define (blah)
  (dict-get %keywords 'xyzzy false))
(list (blah 1 2 3)
      (blah 4 5 :xyzzy 42 8900))
=> (false 42)

;; test closures
(define (make-adder n)
  (lambda (x) (+ x n)))
(define add-two (make-adder 2))
(add-two 9)
=> 11

(define x 0)
(define (foo)
  (set! x 1))
(foo)
x
=> 1

(define (make-adder n)
  (lambda (x) (+ x n )))
(define x 3)
(list ((make-adder x) 4) ((make-adder 12) 4) x)
=> (7 16 3)

(append '(1 2) '(3 4))
=> (1 2 3 4)
(append () ())
=> ()
(append '(1 2) ())
=> (1 2)
(append () '(3 4))
=> (3 4)

;; comment at end
