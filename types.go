package main

import (
    "fmt"
    "regexp"
    "strconv"
    "strings"
)

/* interfaces */

type LispType interface {
    repr() string
    Type() *LispTypeObject
    IsMutable() bool
}

type LispList interface {
    // all the methods that identify LispType need to be here too
    repr() string
    Type() *LispTypeObject
    IsMutable() bool
    // unique to LispList:
    reverse() LispList
    ToSlice() []LispType
    Length() int
}

/* auxiliary types */

type FunctionArgs struct {
    args []LispType
    rest_args []LispType
    keyword_args map[string]LispType
}

type BuiltinFunctionSig func(*Interpreter, *LispEnvironment, *FunctionArgs) (LispType, *Error)
type SpecialFormSig func(*Interpreter, *LispEnvironment, []LispType) (LispType, *Error)

/* structs; i.e., Scorpion List builtin types */

type LispSymbol struct {
    value string
}

type LispPair struct {
    head LispType
    tail LispType
}

type LispEmptyList struct {
}

type LispInteger struct {
    value int
}

type LispString struct {
    value string
}

type LispCharacter struct {
    value rune
}

type LispBuiltinFunction struct {
    name string
    fn BuiltinFunctionSig
    arity int
}

type LispBoolean struct {
    value bool
}

// lambdas currently just take a list of argument names. they don't yet
// support: any number of arguments, keyword arguments, etc.
type LispUserDefinedFunction struct {
    name string  // optional
    argnames []string  
    body []LispType  // a list of expressions
    env *LispEnvironment  // environment it was created in
}

// same contents as LispUserDefinedFunction
//type LispMacro struct { 
//    name string  // optional
//    argnames []string  
//    body []LispType  // a list of expressions
//    env *LispEnvironment  // environment it was created in
//}
type LispMacro LispUserDefinedFunction

type LispTypeObject struct {
    name string
    supertype *LispTypeObject
}

type LispDictionary struct {
    values map[LispType]LispType
}

type LispKeyword struct {
    value string  // value is stored WITHOUT leading ':'
}

type LispEnvironment struct {
    parent *LispEnvironment
    names map[string]LispType
}

/* "constants" (well not really) */

var EMPTY_LIST = LispEmptyList{}
var TRUE = LispBoolean{true}
var FALSE = LispBoolean{false}

/* type names */

func (t LispTypeObject) Type() *LispTypeObject { x, _ := TYPES["type"]; return x }
func (i LispInteger) Type() *LispTypeObject { t, _ := TYPES["int"]; return t }
func (p LispPair) Type() *LispTypeObject { t, _ := TYPES["pair"]; return t }
func (e LispEmptyList) Type() *LispTypeObject { t, _ := TYPES["empty-list"]; return t }
func (b LispBoolean) Type() *LispTypeObject { t, _ := TYPES["bool"]; return t }
func (s LispSymbol) Type() *LispTypeObject { t, _ := TYPES["symbol"]; return t }
func (uf LispUserDefinedFunction) Type() *LispTypeObject { t, _ := TYPES["ufunc"]; return t }
func (bf LispBuiltinFunction) Type() *LispTypeObject { t, _ := TYPES["bfunc"]; return t }
func (m LispMacro) Type() *LispTypeObject { t, _ := TYPES["macro"]; return t }
func (s LispString) Type() *LispTypeObject { t, _ := TYPES["string"]; return t }
func (c LispCharacter) Type() *LispTypeObject { t, _ := TYPES["char"]; return t }
func (d LispDictionary) Type() *LispTypeObject { t, _ := TYPES["dict"]; return t }
func (k LispKeyword) Type() *LispTypeObject { t, _ := TYPES["keyword"]; return t }
func (e LispEnvironment) Type() *LispTypeObject { t, _ := TYPES["environment"]; return t }

/* type hierarchy */

type TypeMap map[string]*LispTypeObject
func TypeHierarchy() TypeMap {
    t := make(TypeMap)
    root := LispTypeObject{"type", nil}
    root.supertype = &root
    t["type"] = &root
    t["int"] = &LispTypeObject{"int", t["type"]}
    t["string"] = &LispTypeObject{"string", t["type"]}
    t["pair"] = &LispTypeObject{"pair", t["type"]}
    t["function"] = &LispTypeObject{"function", t["type"]}
    t["ufunc"] = &LispTypeObject{"ufunc", t["function"]}
    t["bfunc"] = &LispTypeObject{"bfunc", t["function"]}
    t["empty-list"] = &LispTypeObject{"empty-list", t["type"]}
    t["char"] = &LispTypeObject{"char", t["type"]}
    t["symbol"] = &LispTypeObject{"symbol", t["type"]}
    t["macro"] = &LispTypeObject{"macro", t["type"]}
    t["dict"] = &LispTypeObject{"dict", t["type"]}
    t["keyword"] = &LispTypeObject{"keyword", t["type"]}
    t["environment"] = &LispTypeObject{"environment", t["type"]}
    return t
}

var TYPES = TypeHierarchy()

/* repr() */

func (sym LispSymbol) repr() string {
    return sym.value
}
func (pair LispPair) repr() string {
    // traverse list, collecting the repr()s of each head
    // until we reach a pair where the tail is *not* a pair
    // if it's an empty list: this is a proper list
    // otherwise: improper list, use . notation at the end
    var elems []string = make([]string, 0)
    var p *LispPair = &pair
    for {
        // add repr of head to collection
        elems = append(elems, p.head.repr())
        // check type of tail
        if p2, ok := p.tail.(*LispPair); ok {
            p = p2
        } else if _, ok := p.tail.(*LispEmptyList); ok {
            return "(" + strings.Join(elems, " ") + ")"
        } else {
            // improper list
            var s string = strings.Join(elems, " ")
            s = s + " . " + p.tail.repr()
            return "(" + s + ")"
        }
    }
}
func (empty LispEmptyList) repr() string { 
    return "()"
}
func (i LispInteger) repr() string {
    return strconv.Itoa(i.value)
}
func (f LispBuiltinFunction) repr() string {
    return fmt.Sprintf("#<%s>", f.name)
}
func (b LispBoolean) repr() string {
    if b.value { return "true" } else { return "false" }
}
func (f LispUserDefinedFunction) repr() string {
    return "#<lambda>"  // FIXME, later
}
func (m LispMacro) repr() string {
    return "#<macro>"  // FIXME, later
}
func (s LispString) repr() string {
    return fmt.Sprintf("%q", s.value)
}
func (c LispCharacter) repr() string {
    return fmt.Sprintf("#\\%c", c.value)
}
func (t LispTypeObject) repr() string {
    return fmt.Sprintf("@%s", t.name)
}
func (d LispDictionary) repr() string {
    items := d.Items()
    contents := []string{}
    for _, item := range items {
        contents = append(contents, item.repr())
    }
    return fmt.Sprintf("#d(" + strings.Join(contents, " ") + ")")
}
func (k LispKeyword) repr() string {
    return ":" + k.value
}
func (e LispEnvironment) repr() string {
    return "#<environment>"
}

/* IsMutable() */
func (i LispInteger) IsMutable() bool { return false; }
func (s LispString) IsMutable() bool { return false; }
func (t LispTypeObject) IsMutable() bool { return false; }
func (s LispSymbol) IsMutable() bool { return false; }
func (b LispBoolean) IsMutable() bool { return false; }
func (uf LispUserDefinedFunction) IsMutable() bool { return true; }
func (bf LispBuiltinFunction) IsMutable() bool { return false; }
func (e LispEmptyList) IsMutable() bool { return false; }
func (p LispPair) IsMutable() bool { return true; }
func (c LispCharacter) IsMutable() bool { return false; }
func (d LispDictionary) IsMutable() bool { return true; }
func (k LispKeyword) IsMutable() bool { return false; }
func (e LispEnvironment) IsMutable() bool { return true; }
func (m LispMacro) IsMutable() bool { return true; }

/* reverse() */

func (e LispEmptyList) reverse() LispList {
    return &EMPTY_LIST
}
func (pair LispPair) reverse() LispList {
    var acc LispList = &EMPTY_LIST
    
    var p LispType = &pair
    for {
        if p2, ok := p.(*LispPair); ok {
            acc = &LispPair{p2.head, acc}
            p = p2.tail
        } else if _, ok := p.(*LispEmptyList); ok {
            break // all done
        } else {
            panic("cannot reverse improper list")
        }
    }

    return acc
}

/* ToSlice() and FromSlice() */

func (e LispEmptyList) ToSlice() []LispType {
    return []LispType{}
}
func (pair LispPair) ToSlice() []LispType {
    var list []LispType = []LispType{}
    var p LispType = &pair
    for {
        if p2, ok := p.(*LispPair); ok {
            list = append(list, p2.head)
            p = p2.tail
        } else if _, ok := p.(*LispEmptyList); ok {
            return list
        } else {
            panic("Cannot convert improper list to slice")
        }
    }
}

// create a Lisp list from a slice of Lisp expressions.
func FromSlice(stuff []LispType) LispList {
    var p LispList = &EMPTY_LIST
    var l = len(stuff)
    for i := 0; i < len(stuff); i++ {
        p = &LispPair{stuff[l-1-i], p}
    }
    return p
}

/* Length() */

// XXX doesn't necessarily need to be a method; eventually (LENGTH x) will be
// a multimethod, so we'll have a separate version for different types anyway.

func (e LispEmptyList) Length() int { return 0 }
func (p LispPair) Length() int {
    return len(p.ToSlice())
    // FIXME: this will work for now; later, traverse the list
}

/* creating a Lisp object from a token */

var re_integer = regexp.MustCompile(`^-?\d+$`)
var re_string = regexp.MustCompile(`^".*?"$`)
var re_char = regexp.MustCompile(`^#\\.+?$`)
var re_keyword = regexp.MustCompile(`^:.+?$`)

func CreateFromToken(token string) LispType {
    if re_integer.MatchString(token) {
        i, _ := strconv.Atoi(token)
        return &LispInteger{i}
    }
    if re_string.MatchString(token) {
        s, err := strconv.Unquote(token)  // must include quotes
        if err != nil { 
            panic(fmt.Sprintf("Invalid string syntax: %v", token))
        }
        return &LispString{s}
    }
    if re_char.MatchString(token) {
        c := []rune(token)[2]
        return &LispCharacter{c}
    }
    if re_keyword.MatchString(token) {
        return NewKeyword(token)
    }
    // default
    return NewSymbol(token)
}

// use this to create new boolean values. don't use LispBoolean{}!
func ToBoolean(cond bool) *LispBoolean {
    if cond { return &TRUE } else { return &FALSE }
}

// is this Lisp value considered "true"?
// right now, only LispBoolean false is considered "false", everything else is
// considered "true", much like in Scheme. this includes "", 0, (), etc.
func TruthValue(expr LispType) bool {
    if b, ok := expr.(*LispBoolean); ok {
        return b.value
    }
    return true
}

// only create new symbols using this function!
func NewSymbol(s string) *LispSymbol {
    return &LispSymbol{strings.ToLower(s)}
}
// ditto for keywords
func NewKeyword(s string) *LispKeyword {
    if strings.HasPrefix(s, ":") {
        s = s[1:]
    }
    return &LispKeyword{strings.ToLower(s)}
}

