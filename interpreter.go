package main

import (
    "bufio"
    "flag"
    "fmt"
    "io/ioutil"
    "os"
    "path/filepath"
)

type DebugOptions struct {
    show_call_stack bool
    show_tokens bool
}

type Interpreter struct {    
    prompt string
    call_stack *CallStack
    builtin_env *LispEnvironment
    global_env *LispEnvironment
    options DebugOptions
}

func NewInterpreter() Interpreter {
    intp := Interpreter{}
    intp.Init()
    return intp
}

func (intp *Interpreter) Init() {
    intp.prompt = "> "
    intp.call_stack = NewCallStack()
    intp.call_stack.max_stack_depth = 1000
    intp.builtin_env = NewEnvironment()
    intp.global_env = NewEnvironmentWithParent(intp.builtin_env)
    intp.LoadBuiltins()
    intp.AutoLoadCode()
}

func (intp *Interpreter) LoadBuiltins() {
    intp.builtin_env.Set("true", &TRUE)
    intp.builtin_env.Set("false", &FALSE)
    intp.LoadTypes()
    for key, finfo := range builtins {
        intp.builtin_env.Set(key, &LispBuiltinFunction{key, finfo.f, finfo.arity})
        //fmt.Printf("Loading builtin: %q\n", key)
    }
}

func (intp *Interpreter) LoadTypes() {
    for name, t := range TYPES {
        intp.builtin_env.Set("@"+name, t)
    }
}

func (intp *Interpreter) AutoLoadCode() {
    flag.Parse()
    root, _ := filepath.Abs(".")
    // FIXME: use os.Executable() in Go 1.8
    autoload_file := filepath.Join(root, "load", "autoload.sl")
    stuff, err := ioutil.ReadFile(autoload_file)
    if err != nil {
        panic(fmt.Sprintf("Could not read autoload.sl; error: %w\n", err))
    }
    code := string(stuff)
    intp.EvalString(code)
}

func (intp *Interpreter) RunFiles() {
    filenames := flag.Args()
    for _, filename := range(filenames) {
        //fmt.Printf("Executing: %s\n", filename)
        code, _ := ReadFile(filename)
        intp.EvalString(code)
    }
    if len(filenames) > 0 {
        os.Exit(0)
    }
}

func (intp *Interpreter) Welcome() {
    fmt.Printf("Welcome to Scorpion Lisp %s.\n", VERSION)
}

func (intp *Interpreter) MainLoop() {
    rd := bufio.NewReader(os.Stdin)
    reader := NewFileReader(rd)

    for {
        fmt.Print(intp.prompt)
        expr, err := reader.Read()
        if err != nil {
            if err.code == ERR_NO_INPUT { 
                break 
            } else {
                panic(err.Error())
            }
        }

        result := intp.EvalExpr(expr, intp.global_env)
        fmt.Printf("%s\n", result.repr())
    }
}

// process the contents of a string, evaluating all expressions in it, then
// returning a slice with the results of those expressions
// TODO: should really take an environment as additional argument
func (intp *Interpreter) EvalString(s string) []LispType {
    results := []LispType{}
    reader := NewStringReader(s)
    for {
        expr, err := reader.Read()
        if err != nil {
            if err.code == ERR_NO_INPUT { break }
            panic(err.Error())
        }
        result := intp.EvalExpr(expr, intp.global_env)
        results = append(results, result)
    }
    return results
}

func (intp *Interpreter) EvalStep() {
    top := intp.call_stack.Top()

    if top.IsDone() {
        if top.is_atomic {
            value := top.evaluated[0]
            intp.call_stack.Collapse(value)
        } else {
            // if we're done evaluating all the parts, we can call the
            // function. let's do this like Brutus
            callable := top.evaluated[0]
            args := top.evaluated[1:]  // may be empty
            arity := Arity(callable)
            fargs := ParseFunctionArgs(arity, args)
            result := CallFunction(intp, top.env, callable, fargs)
            if result != nil {
               intp.call_stack.Collapse(result)
            }
        }
        return
    }

    // is the expression atomic?
    if top.is_atomic {
        if len(top.evaluated) > 0 {
            // XXX remove this later
            panic("*** this is not supposed to happen")
        }
        result := intp.EvalAtomic(top.original_expr, top.env)
        if top.aux_data != nil {
            top.aux_data.Receive(result)
        } else {
            top.evaluated = append(top.evaluated, result)
        }
    } else {
        // it's a compound expression

        // is it a special form?
        if sym, ok := (top.to_be_evaluated[0]).(*LispSymbol); ok {
            if IsSpecialForm(sym.value) {
                sf, ok := special_forms[sym.value]
                if ok {
                    result, err := sf(intp, top.env, top.to_be_evaluated)
                    if err != nil {
                        panic(err.Error()) // temporary fix
                    }
                    if result != nil {
                        intp.call_stack.Collapse(result)
                    }
                } else {
                    panic(fmt.Sprintf("Unknown special form: %s", sym.value))
                }
                return // leave this
            }
        }

        // evaluate the next element of the compound form by putting it on the
        // call stack
        elem := top.to_be_evaluated[0]
        top.to_be_evaluated = top.to_be_evaluated[1:]
        sf := NewStackFrame(elem, top.env)
        intp.call_stack.Push(sf)
    }
}

// evaluate everything on the call stack, until we get a value back.
func (intp *Interpreter) Eval() LispType {
    for {
        if intp.options.show_call_stack { intp.call_stack.Print() }
        if intp.call_stack.IsDone() {
            // there should only be one value as the final result
            top := intp.call_stack.Top()
            return top.evaluated[0]
        }
        intp.EvalStep()
    }
}

// evaluate an expression by putting it on the call stack and calling eval().
func (intp *Interpreter) EvalExpr(expr LispType, env *LispEnvironment) LispType {
    intp.call_stack.Push(NewStackFrame(expr, env))
    result := intp.Eval()
    intp.call_stack.Clear()
    return result
}

// evaluate a single, atomic value
func (intp *Interpreter) EvalAtomic(expr LispType, env *LispEnvironment) LispType {
    // symbols are looked up
    if sym, ok := expr.(*LispSymbol); ok {
        value, ok2 := env.Get(sym.value)
        if ok2 {
            //fmt.Printf("DEBUG: name %q evaluates to %q\n", sym.value, value)
            return value
        } else {
            panic(fmt.Sprintf("Name not found: %q", sym.value))
        }
    } else {
        // everything else evaluates to itself
        return expr
    }
}

// XXX we can replace this by looking at the keys of special_forms, although
// that might be slower >.>
func IsSpecialForm(s string) bool {
    return (s == "if" || s == "quote" || s == "define" || s == "lambda" || 
            s == "do" || s == "set!" || s == "let" || s == "and" || s == "or" ||
            s == "cond" || s == "macro")
}

// call a function (built-in or user-defined) with the given arguments.
// usually returns a value (the result of the function call); if this is nil,
// however, evaluation will continue via the call stack as usual, allowing us
// to have built-in functions (as opposed to special forms) that manipulate
// the call stack.
// NOTE: assumes that the top element of the call stack contains (the origins
// of) this function call.
func CallFunction(intp *Interpreter, caller_env *LispEnvironment, callable LispType, fargs *FunctionArgs) LispType {
    if f, ok := callable.(*LispBuiltinFunction); ok {
        result, err := f.fn(intp, caller_env, fargs)
        if err != nil {
            panic(err.Error())  // temporary fix
        }
        return result
    } else if f, ok := callable.(*LispUserDefinedFunction); ok {
        // make sure the number of arguments matches
        if len(fargs.args) != len(f.argnames) {
            panic(fmt.Sprintf("Incorrect number of arguments; expected %d, got %d", 
                  len(f.argnames), len(fargs.args)))
        }
        // create new environment based on lambda's environment
        newenv := NewEnvironmentWithParent(f.env)
        // add new values in it
        for i, name := range f.argnames {
            newenv.Set(name, fargs.args[i])
        }
        // create %rest, %keywords variables in new env
        newenv.Set("%rest", FromSlice(fargs.rest_args))
        newenv.Set("%keywords", NewDictionaryFromStringMap(fargs.keyword_args))
        // evaluate the body in this new environment (using the stack)
        // (bodies with multiple parts are wrapped in DO)
        var body LispType
        if len(f.body) <= 1 {
            body = f.body[0]
        } else {
            body = &LispPair{NewSymbol("do"), FromSlice(f.body)}
        }
        sf := NewStackFrame(body, newenv)
        intp.call_stack.Pop() // pop frame containing function call (TCO)
        intp.call_stack.Push(sf) // push new frame w/ body
        return nil  // means, on to next evaluation step
    } else {
        panic(fmt.Sprintf("Not a callable: %q", callable))
    }
}

