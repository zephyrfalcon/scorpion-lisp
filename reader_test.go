package main

import (
    "path/filepath"
    "flag"
    "testing"
)

func TestStringReader(t *testing.T) {
    sr := NewStringReader("1 (2 3) 4")
    exprs := []LispType{}
    for {
        expr, err := sr.Read()
        if err != nil {
            if err.code == ERR_NO_INPUT { break }
            t.Error("This should not happen")
        } else {
            exprs = append(exprs, expr)
        }
    }
    AssertEquals(t, len(exprs), 3)
    AssertEquals(t, exprs[0].repr(), "1")
    AssertEquals(t, exprs[2].repr(), "4")

    // TODO: test unbalanced parenthesis
}

func TestFileReader(t *testing.T) {
    root, _ := filepath.Abs(flag.Arg(0))
    target := filepath.Join(root, "tests", "etc", "1.sl")
    exprs := []LispType{}
    fr := NewFileReaderFromFilename(target)
    for {
        expr, err := fr.Read()
        if err != nil {
            if err.code == ERR_NO_INPUT { break }
            t.Error("This should not happen")
        } else {
            exprs = append(exprs, expr)
        }
    }
    AssertEquals(t, len(exprs), 4)
    AssertEquals(t, exprs[3].repr(), "33")
    AssertEquals(t, exprs[2].repr(), "(baz (quux 4))")
}

func TestFileReaderIncomplete(t *testing.T) {
    root, _ := filepath.Abs(flag.Arg(0))
    target := filepath.Join(root, "tests", "etc", "2.sl")
    exprs := []LispType{}
    fr := NewFileReaderFromFilename(target)
    detected := false  // did we detect the unbalanced paren?

    for {
        expr, err := fr.Read()
        if err != nil {
            if err.code == ERR_NO_INPUT { break }
            if err.code == ERR_UNBALANCED_PAREN { 
                detected = true ; break
            }
            t.Error("This should not happen")
            break
        } else {
            exprs = append(exprs, expr)
        }
    }
    AssertEquals(t, detected, true)
}

func TestFileReaderQuote(t *testing.T) {
    root, _ := filepath.Abs(flag.Arg(0))
    target := filepath.Join(root, "tests", "etc", "3.sl")
    fr := NewFileReaderFromFilename(target)
    
    expr, err := fr.Read()
    AssertEquals(t, err == nil, true)  // t, err, nil) does not work :-/
    AssertEquals(t, expr.repr(), "(quote (1 2))")
}

