package main

import "io/ioutil"
import "reflect"

/* auxiliary functions */

func YesNo(condition bool, yes string, no string) string {
    return Choose(condition, yes, no)
}

func Choose(condition bool, yes string, no string) string {
    if condition {
        return yes
    } else {
        return no
    }
}

func TypeName(x interface{}) string {
    //t := reflect.TypeOf(x)
    //return t.Name()
    v := reflect.ValueOf(x)
    return v.Type().String()
}

func nop(x interface{}) {}

// verify that a list of LispType objects is a list of LispSymbols, and extract
// the values from those symbols, returning them as a list of strings.
func LispTypeListAsNames(values []LispType) []string {
    names := []string{}
    for _, expr := range values {
        if sym, ok := expr.(*LispSymbol); ok {
            names = append(names, sym.value)
        } else {
            panic("list of symbols expected")
        }
    }
    return names
}

func LispTypeListAsReprs(values []LispType) []string {
    reprs := []string{}
    for _, expr := range values {
        reprs = append(reprs, expr.repr())
    }
    return reprs
}

func ReadFile(filename string) (string, error) {
    stuff, err := ioutil.ReadFile(filename)
    if err != nil {
        return "", err
    }
    code := string(stuff)
    return code, nil
}

