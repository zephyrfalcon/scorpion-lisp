package main

import (
    "fmt"
    "reflect"
    "testing"
)

func AssertEquals(t *testing.T, actual interface{}, expected interface{}, context ...interface{}) {
    if !(reflect.DeepEqual(actual, expected)) {
        rest := ""
        if len(context) > 0 {
            rest = fmt.Sprintf("%s", context[0])
        }
        t.Error("Expected:", fmt.Sprintf("%q", expected),
                "Actual:", fmt.Sprintf("%q", actual), rest)
    }
}

func AssertType(t *testing.T, thing interface{}, typename string) {
    //theType := reflect.TypeOf(thing)
    //if theType.Name() != typename {
    v := reflect.ValueOf(thing)
    if v.Type().String() != typename {
        t.Error("Expected type:", fmt.Sprintf("%q", typename),
                "Actual type:", fmt.Sprintf("%q", v.Type().String()))
    }
}

func check_tokens(t *testing.T, actual []string, expected []string) {
    if !(reflect.DeepEqual(actual, expected)) {
        t.Error("Expected:", fmt.Sprintf("%q", expected),
                "Actual:", fmt.Sprintf("%q", actual))
    }
}


